<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/functions_for_bets.php"); ?>
<?php require_once("include/functions_for_results.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Single bet</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th>Match</th>
							<th>Bet</th>
							<th>Match result</th>
							<th>Result</th>
						</tr>
						<?php 
							$bet_SS=getBetById($_GET['id']);
							$matches_SS=getAllMatchesByRoundAndSeason($bet_SS['round'], $bet_SS['season']);
							$matches=array();
							while ($row=mysql_fetch_array($matches_SS)){
								$matches["{$row['id']}"]=$row;
							}
							
							$betArray=parseBets($bet_SS['bet']);
							foreach ($betArray as $bet){
								$names=getJustClubNamesFromMatch($bet['id']);
								echo "<tr>";
								echo "<td>{$names['home']} : {$names['away']}</td>";
								
								if($bet['bet']==4){
									echo "<td>1X</td>";
								}else if($bet['bet']==5){
									echo "<td>X2</td>";
								}else if($bet['bet']==6){
									echo "<td>H(0:1)</td>";
								}else if($bet['bet']==7){
									echo "<td>H(1:0)</td>";
								}else if($bet['bet']==2){
									echo "<td>X</td>";
								}else if($bet['bet']==3){
									echo "<td>2</td>";
								}else{
									echo "<td>1</td>";
								}
								
								
								
								if($bet_SS['result'] == 0){
									echo "<td>-:-</td>";
								}else{
									echo "<td>{$matches["{$bet['id']}"]['home_goals']} : {$matches["{$bet['id']}"]['away_goals']}</td>";
								}
								if($bet_SS['result'] == 0){
									echo "<td>WAIT</td>";
								}else{
									if($matches["{$bet['id']}"]['home_goals']>$matches["{$bet['id']}"]['away_goals']){
										if($bet['bet'] == 1 || $bet['bet'] == 4){
											echo "<td style=\"color:green\">WIN</td>";
										}else if($bet['bet'] == 6 && $matches["{$bet['id']}"]['home_goals']>($matches["{$bet['id']}"]['away_goals']+1)){
											echo "<td style=\"color:green\">WIN</td>";
										}else{
											echo "<td style=\"color:red\">LOST</td>";
										}
									}else if($matches["{$bet['id']}"]['home_goals']<$matches["{$bet['id']}"]['away_goals']){
										if($bet['bet'] == 3  || $bet['bet'] == 5){
											echo "<td style=\"color:green\">WIN</td>";
										}else if($bet['bet'] == 7 && ($matches["{$bet['id']}"]['home_goals']+1)<$matches["{$bet['id']}"]['away_goals']){
											echo "<td style=\"color:green\">WIN</td>";
										}else{
											echo "<td style=\"color:red\">LOST</td>";
										}
									}else{
										if($bet['bet'] == 2|| $bet['bet'] == 4 || $bet['bet'] == 5){
											echo "<td style=\"color:green\">WIN</td>";
										}else{
											echo "<td style=\"color:red\">LOST</td>";
										}
									}
								}
								echo "<tr>";
							}
						
						?>
					</table>
					Place bet:<?php echo $bet_SS['bet_place']; ?>
					<span style="padding-left: 50px;<?php if ($bet_SS['result']==1) echo "color:green";?>">Price: <?php echo round(($bet_SS['bet_place']*$bet_SS['koef']),2); ?></span>
					<span style="padding-left: 50px;<?php if ($bet_SS['result']==3) echo "color:green";?>">Half price: <?php echo round(($bet_SS['bet_place']*$bet_SS['koef']/$bet_SS['max_koef']),2); ?></span>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>