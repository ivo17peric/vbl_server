<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
	//reset league table
	$query="UPDATE league_table SET
		points=0, win_games=0, draw_games=0, lose_games=0,
		games=0, goals_scored=0, goals_allowed=0";
	$result = mysql_query($query, $conn);
	
	//reset game
	$query="UPDATE game SET
		current_season=1, current_round=-1, date_current='', date_next='', date_after_next='' WHERE id=1;";
	$result = mysql_query($query, $conn);
	
	//reset clubs form
	$query="UPDATE clubs SET
		form=5, last_five_form='00000'";
	$result = mysql_query($query, $conn);
	
	//clear all match
	$query="DELETE FROM matches";
	$result = mysql_query($query, $conn);
	
	//clear all bets
	$query="DELETE FROM bets";
	$result = mysql_query($query, $conn);
	
	//reset users
	$query="UPDATE users SET
		p_points=100, points=0, points_ten_games=0, points_ten_games_last=0";
	$result = mysql_query($query, $conn);
	
	redirect_to("current_round.php");

?>