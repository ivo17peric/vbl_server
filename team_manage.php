<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
include_once("include/form_function.php");

if (isset($_POST['submit'])){
	$errors = array();
	// perform validations on the form data
	$required_fields = array();
	for($i=0;$i<16;$i++){
		array_push($required_fields, "team".($i+1));
		array_push($required_fields, "short_team".($i+1));
		array_push($required_fields, "offence".($i+1));
		array_push($required_fields, "defence".($i+1));
	}
	$errors = array_merge($errors, check_required_fields($required_fields, $_POST));

	for($i=0;$i<16;$i++){
		if(intval($_POST['defence'.($i+1)])>1000 || intval($_POST['defence'.($i+1)])<1){
			$err=array();
			$err[]=$_POST['team'.($i+1)]." has incorrect defence (1-1000)";
			$errors=array_merge($errors,$err);
		}
	}
	
	for($i=0;$i<16;$i++){
		if(intval($_POST['offence'.($i+1)])>1000 || intval($_POST['offence'.($i+1)])<1){
			$err=array();
			$err[]=$_POST['team'.($i+1)]." has incorrect offence (1-1000)";
			$errors=array_merge($errors,$err);
		}
	}
	$message="";
	if ( empty($errors) ) {
		for($i=1;$i<17;$i++){
			$name=trim(mysql_prep($_POST['team'.$i]));
			$short_name=trim(mysql_prep($_POST['short_team'.$i]));
			$offence=trim(mysql_prep($_POST['offence'.$i]));
			$defence=trim(mysql_prep($_POST['defence'.$i]));
			$indexQ=round(($offence+$defence)/100);
			$query = "UPDATE clubs 
					SET name='{$name}',
					short_name='{$short_name}',
					offence={$offence},
					defence={$defence},
					index_quality={$indexQ}
					WHERE id={$i}";
			$result = mysql_query($query, $conn);
			if ($result) {
				if($message==""){
					$message .= "The team ";
				}
				if($i!=16){
					$message .= "{$i}, ";
				}else{
					$message .= "{$i} were successfully created.";
				}
			} else {
				$message .= "The team {$i} could not be created.";
				$message .= "<br />" . mysql_error();
			} 
		}
	}else {
		if (count($errors) == 1) {
			$message .= "There was 1 error in the form.";
		} else {
			$message .= "There were " . count($errors) . " errors in the form.";
		}
	}
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Team manage</h3>
				</div>
				<div class="panel-body">
					<form action="team_manage.php" method="post">
						<table class="table table-bordered">
							<tr>
								<td>ID</td>
								<td>ID_s</td>
								<td>Name</td>
								<td>Short</td>
								<td>OFF</td>
								<td>DEF</td>
								<td>IQ</td>
								<td>Form</td>
								<td>Last 5</td>
							</tr>
							<?php 
								$clubs=getClubs();
								$game=getGame();
								$i=1;
								while ($row=mysql_fetch_array($clubs)){
									echo "<tr>";
									echo "<td>{$row['id']}</td>";
									echo "<td>{$row['id_schedule']}</td>";
									// if round is not -1
									if($game['current_round']!=-1){
										echo "<td>{$row['name']}</td>";
									}else{
									?>
										<td><input type="text" class="form-control" 
												name="<?php echo "team".$i ?>" maxlength="30" 
												value="<?php echo "{$row['name']}" ?>">
										</td>
									<?php
									} 
									if($game['current_round']!=-1){
										echo "<td>{$row['short_name']}</td>";
									}else{
									?>
										<td><input type="text" class="form-control" style="width: 60px"
												name="<?php echo "short_team".$i ?>" maxlength="3" 
												value="<?php echo "{$row['short_name']}" ?>">
										</td>
									<?php
									}
									?>
										<td><input type="text" class="form-control" style="width: 60px"
												name="<?php echo "offence".$i ?>" maxlength="4" 
												value="<?php echo "{$row['offence']}" ?>">
										</td>
										<td><input type="text" class="form-control" style="width: 60px"
													name="<?php echo "defence".$i ?>" maxlength="4" 
													value="<?php echo "{$row['defence']}" ?>">
										</td>
									<?php
									echo "<td>{$row['index_quality']}</td>";
									$form=round($row['form'], 2);
									echo "<td>{$form}</td>";
									echo "<td>";//{$row['last_five_form']}</td>";
									for($j=0;$j<5;$j++){
										if($row['last_five_form']{$j}==3){
											echo "<span style='color: #81D2FF'>D</span>";
										} else if($row['last_five_form']{$j}==1){
											echo "<span style='color: #9DF165'>O</span>";
										} else if($row['last_five_form']{$j}==2){
											echo "<span style='color: #FF7B85'>X</span>";
										}else {
											echo "-";
										}
									}
									echo "</td>";
									$i++;
									echo "<tr>";
								}
							?>
						</table>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
							value="Update clubs" >
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>