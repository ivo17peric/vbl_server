<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$uuid = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['uuid'])) $uuid =  $_GET['uuid'];
	}else{
		if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	}
	
	$page=1;
	if(isset($_GET['page'])){
		$page=$_GET['page'];
		if($page<-1){
			$page=1;
		}
	}
	
	if(isset($_POST['page'])){
		$page=$_POST['page'];
		if($page<-1){
			$page=1;
		}
	}

	$userData = getUserByUuid($uuid);

	$bets_set=getAllBetsByUserId(($page-1)*10, 10, $userData['id']);

	if($uuid == -1){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}

	if(sizeof($bets_set) == 0){
		echo createJsonResponse(null, 'data', 0, "NO BETS FOR USER");
		return;
	}

	$response=array();

	while ($row = mysql_fetch_array($bets_set, MYSQL_ASSOC)) {
		array_push($response,$row);
	}

	$number = getNumberOffAllBetsByUserId($userData['id']);

	echo createJsonResponse(array('bets' => $response, 'user' => $userData, 'bet_num' => $number), 'data', 1, "");

?>