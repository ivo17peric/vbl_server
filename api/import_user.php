<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	
	$uuid="-1"; $userId = -1; $password = "-1"; $deviceId = "-1";
	
	if(isset($_GET['uuid'])) $uuid = $_GET['uuid'];
	if(isset($_POST['uuid'])) $uuid = $_POST['uuid'];
	if(isset($_GET['password'])) $password = $_GET['password'];
	if(isset($_POST['password'])) $password = $_POST['password'];
	if(isset($_GET['user_id'])) $userId = $_GET['user_id'];
	if(isset($_POST['user_id'])) $userId = $_POST['user_id'];
	if(isset($_GET['deviceId'])) $deviceId = $_GET['deviceId'];
	if(isset($_POST['deviceId'])) $deviceId = $_POST['deviceId'];
	
	
	if($uuid == "-1"){
		echo createJsonResponse(null, 'data', 0, "UUID IS WRONG");
		return;
	}
	
	if($userId == -1){
		echo createJsonResponse(null, 'data', 0, "USER ID IS WRONG");
		return;
	}
	
	if($password == "-1"){
		echo createJsonResponse(null, 'data', 0, "PASSWORD IS WRONG");
		return;
	}
	
	$userExport = getExportUserByIdAndPass($userId, $password);
	if($userExport == null){
		echo createJsonResponse(null, 'data', 0, "WRONG COMBINATION");
		return;
	}
	
	if($uuid == $userExport['uuid']){
		echo createJsonResponse(null, 'data', 0, "Same uuid!!");
		return;
	}
	
	$userToRemove = getUserByUuid($uuid);
	if($userToRemove == null){
		echo createJsonResponse(null, 'data', 0, "NO USER FOUND");
		return;
	}
	
	$userToUpdate = getUserByUuid($userExport['uuid']);
	if($userToUpdate == null){
		echo createJsonResponse(null, 'data', 0, "NO EXPORT USER FOUND");
		return;
	}
	
	$query = "UPDATE users SET
	uuid = '{$uuid}',
	device_id = '{$deviceId}'
	WHERE id = {$userToUpdate['id']}";
	$result = mysql_query($query, $conn);
	
	if ($result) {
		$userIdToRemove = $userToRemove['id'];
		$query = "DELETE FROM users WHERE id = {$userIdToRemove};";
		$result = mysql_query($query, $conn);
	
		$query = "DELETE FROM export WHERE user_id = {$userExport['user_id']};";
		$result = mysql_query($query, $conn);
	
		echo createJsonResponse(null, 'data', 1, "User imported!!");
	} else {
		echo createJsonResponse(null, 'data', 0, "FAILED");
	}

?>