<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php require_once("../include/functions_for_bets.php"); ?>
<?php require_once("../include/functions_for_results.php"); ?>
<?php 

	$offset = 0;
	$round = -1;
	$season = -1;
	$uuid = -1;
	$place_bet = -1;
	$max_coeff = 1;
	$coef = 1;
	$bet = "";
	
	if(isset($_POST['offset'])) $offset = $_POST['offset'];
	if(isset($_POST['round'])) $round =  $_POST['round'];
	if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	if(isset($_POST['place_bet'])) $place_bet =  $_POST['place_bet'];
	if(isset($_POST['max_coef'])) $max_coeff =  $_POST['max_coef'];
	if(isset($_POST['coef'])) $coef =  $_POST['coef'];
	if(isset($_POST['bet'])) $bet =  $_POST['bet'];

	if($round == -1){
		echo createJsonResponse(null, 'data', 0, "NO ROUND DEFINED");
		return;
	}

	if($uuid == -1 || $place_bet == -1){
		echo createJsonResponse(null, 'data', 0, "NO USER ID OR PLACE BET");
		return;
	}

	$user=getUserByUuid($uuid);
	$userId = $user['id'];
	$seasonT = getGame();
	$season = $seasonT['current_season'];

	$coef = round($coef, 2);
	$max_coeff = round($max_coeff, 2);

	if(strlen($bet)==0 || $_POST['place_bet']<1){
		echo createJsonResponse(null, 'data', 0, "NOTHING TO BET");
		return;
	}else if($user['p_points'] < $place_bet){
		echo createJsonResponse(null, 'data', 0, "NOT ENOUGHT POINT");
		return;
	}else{
		$query="INSERT INTO bets (bet_place, season, round, user_id, bet, koef, max_koef)
		VALUES ({$place_bet}, {$season}, {$round}, {$user['id']}, '{$bet}', {$coef}, {$max_coeff})";
		$result=mysql_query($query, $conn);

		if ($result) {
			$message .= "The bet was successfully inserted.<br />";
			$message=updateUsersPPoints($userId, (-$place_bet), $message, $conn);
			echo createJsonResponse(null, 'data', 2, "Bet is added.");
			return;
		} else {
			$message .= "The bet could not be inserted.";
			$message .= "<br />" . mysql_error()."<br />";
			echo createJsonResponse(null, 'data', 0, $message);
			return;
		}
	}
	echo createJsonResponse(null, 'data', 1, "SUCCESS");

?>