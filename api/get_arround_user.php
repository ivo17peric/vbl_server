<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	$userId = -1;
	$toBelow = 1;
	$uuid = -1;
	
	if(isset($_GET['orderBy'])) $orderBy = $_GET['orderBy'];
	if(isset($_POST['orderBy'])) $orderBy = $_POST['orderBy'];
	if(isset($_GET['user_id'])) $userId =  $_GET['user_id'];
	if(isset($_POST['user_id'])) $userId =  $_POST['user_id'];
	if(isset($_GET['to_below'])) $toBelow =  $_GET['to_below'];
	if(isset($_POST['to_below'])) $toBelow =  $_POST['to_below'];
	if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	
	$page=1;
	if(isset($_GET['page'])){
		$page=$_GET['page'];
		if($page<-1){
			$page=1;
		}
	}

	if($userId == -1 && $uuid == -1){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}
	if($userId == -1){
		$userData = getUserByUuid($uuid);
		$userId = $userData['id'];
	}else{
		$userData = getUserById($userId);
	}

	if($page == 1){
		$user_set_below=getAllUsersBelowBy(0, 4, $orderBy, $userId, $userData[$orderBy]);
		$user_set_above=getAllUsersAboveBy(0, 5, $orderBy, $userId, $userData[$orderBy]);
			
		$response=array();

		while ($row = mysql_fetch_array($user_set_above, MYSQL_ASSOC)) {
			array_push($response,$row);
		}
			
		$response = reoderArray($response);
		array_push($response,$userData);
			
		while ($row = mysql_fetch_array($user_set_below, MYSQL_ASSOC)) {
			array_push($response,$row);
		}

		echo createJsonResponse($response, 'data', 1, "");
			
	}else{
			
		if($toBelow == 1){
			if($page == 2){
				$user_set=getAllUsersBelowBy(4, 10, $orderBy, $userId, $userData[$orderBy]);
			}else{
				$user_set=getAllUsersBelowBy(4 + ($page-2)*10, 10, $orderBy, $userId, $userData[$orderBy]);
			}
		}else{
			if($page == 2){
				$user_set=getAllUsersAboveBy(5, 10, $orderBy, $userId, $userData[$orderBy]);
			}else{
				$user_set=getAllUsersAboveBy(5+($page-2)*10, 10, $orderBy, $userId, $userData[$orderBy]);
			}
		}
			
		if(sizeof($user_set) == 0){
			echo createJsonResponse(null, 'data', 0, "NO USERS");
			return;
		}
			
		$response=array();
			
		while ($row = mysql_fetch_array($user_set, MYSQL_ASSOC)) {
			array_push($response,$row);
		}
			
		if($toBelow != 1){
			$response = reoderArray($response);
		}
			
		echo createJsonResponse($response, 'data', 1, "");
			
	}

?>