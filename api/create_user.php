<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	
	$uuid="-1";$email="-1";$deviceId="-1";$user="-1";

	if(isset($_GET['uuid'])) $uuid = $_GET['uuid'];
	if(isset($_POST['uuid'])) $uuid = $_POST['uuid'];
	if(isset($_GET['email'])) $email = $_GET['email'];
	if(isset($_POST['email'])) $email = $_POST['email'];
	if(isset($_GET['deviceId'])) $deviceId = $_GET['deviceId'];
	if(isset($_POST['deviceId'])) $deviceId = $_POST['deviceId'];
	if(isset($_GET['username'])) $user = $_GET['username'];
	if(isset($_POST['username'])) $user = $_POST['username'];
	
	if($uuid == "-1"){
		echo createJsonResponse(null, 'data', 0, "UUID IS WRONG");
		return;
	}

	if($email == "-1"){
		echo createJsonResponse(null, 'data', 0, "EMAIL IS WRONG");
		return;
	}

	if($deviceId == "-1"){
		echo createJsonResponse(null, 'data', 0, "DEVICEID IS WRONG");
		return;
	}

	if($user == "-1"){
		echo createJsonResponse(null, 'data', 0, "USERNAME IS WRONG");
		return;
	}

	if(checkIfUserIsTakken($user)){
		echo createJsonResponse(null, 'data', -2, "USERNAME IS ALLREADY EXISTS");
		return;
	}

	$last_login=date('d-m-Y, H:i:s');

	$query = "INSERT INTO users (uuid, email, username, is_vip, vip_till, last_login,
				device_id, first_login, p_points, points, points_ten_games,
				points_ten_games_last)
				VALUES ('{$uuid}', '{$email}', '{$user}', 0, '', '{$last_login}', '{$deviceId}',
				'{$last_login}', 100, 0, 0, 0 )";

	$result = mysql_query($query, $conn);
	if ($result) {
		echo createJsonResponse(null, 'data', 1, "USER ADDED");
	} else {
		echo createJsonResponse(null, 'data', 0, "FAILED");
	}
		
?>