<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$userId = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['user_id'])) $userId =  $_GET['user_id'];
	}else{
		if(isset($_POST['user_id'])) $userId =  $_POST['user_id'];
	}

	$userData = getUserById($userId);

	if($userId == -1){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}

	if(sizeof($userData) == 0){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}

	echo createJsonResponse($userData, 'data', 1, "");

?>