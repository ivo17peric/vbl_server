<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php require_once("../include/functions_for_bets.php"); ?>
<?php require_once("../include/functions_for_results.php"); ?>
<?php 
	$offset = 0;
	$round = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['offset'])) $offset = $_GET['offset'];
		if(isset($_GET['round'])) $round =  $_GET['round'];
	} else {
		if(isset($_POST['offset'])) $offset = $_POST['offset'];
		if(isset($_POST['round'])) $round =  $_POST['round'];
	}
	
	if(isset($_POST['uuid'])) $uuid = $_POST['uuid'];
	if(isset($_GET['uuid'])) $uuid =  $_GET['uuid'];

	$game=getGame();

	$current=0;
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}

	if($round == -1) {
		$round = $current + $offset+1;
	}
	
	if($round <= $current){
		echo createJsonResponse(null, 'data', 0, "ROUND IS ALLREADY PLAYED");
		return;
	}

	$allArray = array();

	for($i=1;$i<9;$i++){
		$match_SS=getMatchesWithAllInfomation($round, $game['current_season'], $i);
		//while - for home and away team information
		while ($match = mysql_fetch_array($match_SS)){
			if($match[0] == $match['home_club_id']){
				$homeTeam=$match;
			}else{
				$awayTeam=$match;
			}
		}
			
		$coeff=getCoeff(getPercetangeForTeam($homeTeam, $awayTeam));
		$coeffArray = array('home_win' => $coeff[0],
				'draw' => $coeff[1],
				'away_win' => $coeff[2],
				'home_draw_win' => $coeff[3],
				'away_draw_win' => $coeff[4],
				'home_hendicap' => $coeff[5],
				'away_hendicap' => $coeff[6]
		);

		array_push($allArray, array('season' => $homeTeam['season'],
		'round' => $homeTeam['round'],
		'home_name' => $homeTeam['name'],
		'home_offence' => $homeTeam['offence'],
		'home_defence' => $homeTeam['defence'],
		'home_last_form' => $homeTeam['last_five_form'],
		'home_form' => $homeTeam['form'],
		'away_name' => $awayTeam['name'],
		'away_offence' => $awayTeam['offence'],
		'away_defence' => $awayTeam['defence'],
		'away_last_form' => $awayTeam['last_five_form'],
		'away_form' => $awayTeam['form'],
		'bets' => $coeffArray,
		'date_match' => $homeTeam['date_match'],
		'match_id' => $homeTeam['id']
		));
			
	}

	$userData = getUserByUuid($uuid);

	echo createJsonResponse(array('bets' => $allArray, 'p_points' => $userData['p_points']), 'data', 1, "");

?>