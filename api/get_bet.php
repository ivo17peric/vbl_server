<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php require_once("../include/functions_for_bets.php"); ?>
<?php require_once("../include/functions_for_results.php"); ?>
<?php 
	$betId = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['bet_id'])) $betId =  $_GET['bet_id'];
	}else{
		if(isset($_POST['bet_id'])) $betId =  $_POST['bet_id'];
	}
	
	$bet_set=getBetById($betId);

	if($betId == -1){
		echo createJsonResponse(null, 'data', 0, "NO BET ID");
		return;
	}

	$matches_SS=getAllMatchesByRoundAndSeason($bet_set['round'], $bet_set['season']);
	while ($row=mysql_fetch_array($matches_SS)){
		$matches["{$row['id']}"]=$row;
	}
	$betArray=parseBets($bet_set['bet']);

	$bets = array();
	foreach ($betArray as $bet){
			
		$names=getJustClubNamesFromMatch($bet['id']);
			
		array_push($bets, array('home_name' => $names['home'],
		'away_name' => $names['away'],
		'bet' => $bet['bet'],
		'result' => $bet_set['result'],
		'home_goals' => $matches["{$bet['id']}"]['home_goals'],
		'away_goals' => $matches["{$bet['id']}"]['away_goals'],
		'bet_place' => $bet_set['bet_place'],
		'koef' => $bet_set['koef'],
		'price' => round(($bet_set['bet_place']*$bet_set['koef']),2),
		'half_price' => round(($bet_set['bet_place']*$bet_set['koef']/$bet_set['max_koef']),2)));
	}

	if(sizeof($bets) == 0){
		echo createJsonResponse(null, 'data', 0, "NO BET");
		return;
	}

	echo createJsonResponse($bets, 'data', 1, "");
?>