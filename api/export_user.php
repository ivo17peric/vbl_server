<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	
	$uuid="-1";

	if(isset($_GET['uuid'])) $uuid = $_GET['uuid'];
	if(isset($_POST['uuid'])) $uuid = $_POST['uuid'];
	
	if($uuid == "-1"){
		echo createJsonResponse(null, 'data', 0, "UUID IS WRONG");
		return;
	}

	$userData = getUserByUuid($uuid);
	$userId = $userData['id'];
	$dateExported = date('d-m-Y, H:i:s');
	$password = $_POST['password'];
	$hashedPassword = sha1($password);

	$query = "INSERT INTO export (uuid, user_id, password, date_exported)
	VALUES ('{$uuid}', $userId, '{$hashedPassword}','{$dateExported}')";

	$result = mysql_query($query, $conn);
	if ($result) {
		echo createJsonResponse(null, 'data', 1, "User exported data saved!!");
	} else {
		echo createJsonResponse(null, 'data', 0, "FAILED");
	}
		
?>