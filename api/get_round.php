<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$offset = 0;
	$round = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['offset'])) $offset = $_GET['offset'];
		if(isset($_GET['round'])) $round =  $_GET['round'];
	}else{
		if(isset($_POST['offset'])) $offset = $_POST['offset'];
		if(isset($_POST['round'])) $round =  $_POST['round'];
	}

	$game=getGame();

	$current=1;
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}

	if($round == -1) $round = $current + $offset;
	$matches_set=getMatchesWithClubNameByRoundAndSeason($round, $game['current_season']);

	$response=array();

	while ($row = mysql_fetch_array($matches_set, MYSQL_ASSOC)) {
		array_push($response,$row);
	}

	if(sizeof($response) == 0){
		echo createJsonResponse(null, 'data', 0, "CALCULATING ROUND");
		return;
	}

	echo createJsonResponse($response, 'data', 1, "");

?>