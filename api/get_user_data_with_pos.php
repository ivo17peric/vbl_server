<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$userId = -1;
	$uuid = -1;
	
	if($_GET['get'] == 1) {
		if(isset($_GET['user_id'])) $userId =  $_GET['user_id'];
	}else{
		if(isset($_POST['user_id'])) $userId =  $_POST['user_id'];
	}
	if($_GET['get'] == 1) {
		if(isset($_GET['uuid'])) $uuid =  $_GET['uuid'];
	}else{
		if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	}
	
	if($userId == -1){
		$userData = getUserByUuid($uuid);
		$userId = &$userData['id'];
	}else{
		$userData = getUserById($userId);
	}

	if($userId == -1 && $uuid == -1){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}

	if(sizeof($userData) == 0){
		echo createJsonResponse(null, 'data', 0, "NO USER ID");
		return;
	}

	$userPosition = 1 + getNumberOfUsersAboveBy("p_points", $userId, $userData['p_points']);
	$userTenGamesPosition = 1 + getNumberOfUsersAboveBy("points_ten_games", $userId, $userData['points_ten_games']);
	$userLastTenGamesPosition = 1 + getNumberOfUsersAboveBy("points_ten_games_last", $userId, $userData['points_ten_games_last']);
	$userAllTimePosition = 1 + getNumberOfUsersAboveBy("points", $userId, $userData['points']);

	$userData['user_p_points_pos'] = $userPosition;
	$userData['user_points_pos'] = $userAllTimePosition;
	$userData['user_ten_games_pos'] = $userTenGamesPosition;
	$userData['user_last_ten_games_pos'] = $userLastTenGamesPosition;

	echo createJsonResponse($userData, 'data', 1, "");

?>