<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	
	$userId = -1;
	
	if(isset($_GET['orderBy'])) $orderBy = $_GET['orderBy'];
	if(isset($_POST['orderBy'])) $orderBy = $_POST['orderBy'];
	if(isset($_GET['user_id'])) $userId =  $_GET['user_id'];
	if(isset($_POST['user_id'])) $userId =  $_POST['user_id'];
	if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	
	$page=1;

	//TOP USERS
	$user_set=getAllUsersBy(($page-1)*10, 10, $orderBy);

	$responseTop=array();

	while ($row = mysql_fetch_array($user_set, MYSQL_ASSOC)) {
		array_push($responseTop,$row);
	}

	//AROUND USERS
	if($userId == -1){
		$userData = getUserByUuid($uuid);
		$userId = $userData['id'];
	}else{
		$userData = getUserById($userId);
	}

	$user_set_below=getAllUsersBelowBy(0, 4, $orderBy, $userId, $userData[$orderBy]);
	$user_set_above=getAllUsersAboveBy(0, 5, $orderBy, $userId, $userData[$orderBy]);

	$pos_my = 1 + getNumberOfUsersAboveBy($orderBy, $userId, $userData[$orderBy]);
		
	$responseAround=array();

	while ($row = mysql_fetch_array($user_set_above, MYSQL_ASSOC)) {
		array_push($responseAround,$row);
	}
		
	$responseAround = reoderArray($responseAround);
	$userData['pos_my'] = $pos_my;
	array_push($responseAround,$userData);
		
	while ($row = mysql_fetch_array($user_set_below, MYSQL_ASSOC)) {
		array_push($responseAround,$row);
	}

	$response = array('top' => $responseTop,
			'around' => $responseAround
	);

	echo createJsonResponse($response, 'data', 1, "");

?>