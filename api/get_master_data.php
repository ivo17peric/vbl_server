<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$userId = -1;
	$uuid = -1;
	if($_GET['get'] == 1) {
		if(isset($_GET['user_id'])) $userId =  $_GET['user_id'];
		if(isset($_GET['uuid'])) $uuid =  $_GET['uuid'];
	}else{
		if(isset($_POST['user_id'])) $userId =  $_POST['user_id'];
		if(isset($_POST['uuid'])) $uuid =  $_POST['uuid'];
	}

	$game=getGame();

	//current round
	$current=1;
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}

	if($game['current_round'] == -1 || $current == 30){
		$round = $current;
	}else{
		$round = $current+1;
	}

	$matches_set=getMatchesWithClubNameByRoundAndSeason($round, $game['current_season']);

	$currentRoundResponse = array();

	while ($row = mysql_fetch_array($matches_set, MYSQL_ASSOC)) {
		array_push($currentRoundResponse,$row);
	}

	//next round
	$nextRoundResponse = array();
	if($game['current_round'] == -1){
		$round = $current+1;
	}else{
		$round = $current+2;
	}
	if($round<31){
		$matches_set=getMatchesWithClubNameByRoundAndSeason($round, $game['current_season']);
			
		while ($row = mysql_fetch_array($matches_set, MYSQL_ASSOC)) {
			array_push($nextRoundResponse,$row);
		}
	}

	//previuos round
	$prevRoundResponse = array();
	if($current == 30){
		$round = $current - 1;
	}else {
		$round = $current;
	}

	if($game['current_round'] != -1){
		$matches_set=getMatchesWithClubNameByRoundAndSeason($round, $game['current_season']);

		while ($row = mysql_fetch_array($matches_set, MYSQL_ASSOC)) {
			array_push($prevRoundResponse,$row);
		}
	}

	//table
	$table=getTable();

	$respTable=array();

	while ($row = mysql_fetch_array($table, MYSQL_ASSOC)) {
		array_push($respTable,$row);
	}

	//user data
	if($userId == -1){
		$userData = getUserByUuid($uuid);
		$userId = $userData['id'];
	}else{
		$userData = getUserById($userId);
	}

	if(!isset($userData['id'])){
		echo createJsonResponse(null, 'data', 0, "WRONG USER ID");
		return;
	}

	//bets data
	$bets_set=getAllBetsByUserId(0, 5, $userId);

	$responseBets=array();

	while ($row = mysql_fetch_array($bets_set, MYSQL_ASSOC)) {
		array_push($responseBets,$row);
	}

	//users data p points
	$user_set_below=getAllUsersBelowBy(0, 4, 'p_points', $userId, $userData['p_points']);
	$user_set_above=getAllUsersAboveBy(0, 5, 'p_points', $userId, $userData['p_points']);

	$responseUsersByPPoints=array();

	while ($row = mysql_fetch_array($user_set_above, MYSQL_ASSOC)) {
		array_push($responseUsersByPPoints,$row);
	}
		
	$responseUsersByPPoints = reoderArray($responseUsersByPPoints);
	array_push($responseUsersByPPoints,$userData);
		
	while ($row = mysql_fetch_array($user_set_below, MYSQL_ASSOC)) {
		array_push($responseUsersByPPoints,$row);
	}

	//user data points
	$user_set_below=getAllUsersBelowBy(0, 4, 'points', $userId, $userData['points']);
	$user_set_above=getAllUsersAboveBy(0, 5, 'points', $userId, $userData['points']);

	$responseUsersByPoints=array();

	while ($row = mysql_fetch_array($user_set_above, MYSQL_ASSOC)) {
		array_push($responseUsersByPoints,$row);
	}
		
	$responseUsersByPoints = reoderArray($responseUsersByPoints);
	array_push($responseUsersByPoints,$userData);
		
	while ($row = mysql_fetch_array($user_set_below, MYSQL_ASSOC)) {
		array_push($responseUsersByPoints,$row);
	}

	//user data last ten
	$user_set_below=getAllUsersBelowBy(0, 4, 'points_ten_games', $userId, $userData['points_ten_games']);
	$user_set_above=getAllUsersAboveBy(0, 5, 'points_ten_games', $userId, $userData['points_ten_games']);

	$responseUsersByPointsTenGames=array();

	while ($row = mysql_fetch_array($user_set_above, MYSQL_ASSOC)) {
		array_push($responseUsersByPointsTenGames,$row);
	}
		
	$responseUsersByPointsTenGames = reoderArray($responseUsersByPointsTenGames);
	array_push($responseUsersByPointsTenGames,$userData);
		
	while ($row = mysql_fetch_array($user_set_below, MYSQL_ASSOC)) {
		array_push($responseUsersByPointsTenGames,$row);
	}

	$response = array('current_round' => $currentRoundResponse,
			'next_round' => $nextRoundResponse,
			'prev_round' => $prevRoundResponse,
			'table' => $respTable,
			'user' => $userData,
			'bets' => $responseBets,
			'users_p_points' => $responseUsersByPPoints,
			'users_points' => $responseUsersByPoints,
			'users_ten_games' => $responseUsersByPointsTenGames
	);

	echo createJsonResponse($response, 'data', 1, "");

?>