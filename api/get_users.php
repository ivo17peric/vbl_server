<?php require_once("../include/connection.php"); ?>
<?php require_once("../include/functions.php"); ?>
<?php 
	$orderBy = 'id';
	
	if(isset($_GET['orderBy'])) $orderBy = $_GET['orderBy'];
	if(isset($_POST['orderBy'])) $orderBy = $_POST['orderBy'];
	
	$page=1;
	if(isset($_GET['page'])){
		$page=$_GET['page'];
		if($page<-1){
			$page=1;
		}
	}
		
	$user_set=getAllUsersBy(($page-1)*10, 10, $orderBy);
		
	if(sizeof($user_set) == 0){
		echo createJsonResponse(null, 'data', 0, "NO USERS");
		return;
	}
		
	$response=array();
		
	while ($row = mysql_fetch_array($user_set, MYSQL_ASSOC)) {
		array_push($response,$row);
	}
		
	echo createJsonResponse($response, 'data', 1, "");
		

?>