<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/functions_for_bets.php"); ?>
<?php require_once("include/functions_for_results.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
if (isset($_POST['submit'])){
	
	$message="";
	
	$game=getGame();
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}
	
	$round=$current+1;
	
	$betsString="";
	$coeff_SS=1;
	$max_coeff=0;
	for($i=0;$i<8;$i++){
		$match_id=$_POST['match'.$i];
		$betCoeff=$_POST['bet'.$i];
		$bet=substr($betCoeff, 0, 1);
		$coeff_KK=substr($betCoeff, 2);
		if($bet!=0){
			$betsString .=$match_id."?".$bet.";";
			$coeff_SS=$coeff_SS*$coeff_KK;
			if($coeff_KK>$max_coeff) $max_coeff=$coeff_KK;
		}
	}
	
	$userId=$_POST['user_id'];
	$user=getUserById($userId);
	$place_bet=$_POST['place_bet'];
	
	if(strlen($betsString)==0 || $_POST['place_bet']<1){
		echo "Nothing to bet";
	}else if($user['p_points'] < $place_bet){
		echo "User doesn't have enough points";
	}else{
		$betsString=substr($betsString, 0, -1);
		$userId=$_POST['user_id'];
		$place_bet=$_POST['place_bet'];
		$coeff_SS=round($coeff_SS, 2);
		$max_coeff=round($max_coeff, 2);
		
		$query="INSERT INTO bets (bet_place, season, round, user_id, bet, koef, max_koef)
			VALUES ({$place_bet}, {$game['current_season']}, {$round}, {$userId}, '{$betsString}', {$coeff_SS}, {$max_coeff})";
		$result=mysql_query($query, $conn);
		
		if ($result) {
			$message .= "The bet was successfully inserted.<br />";
			$message=updateUsersPPoints($userId, (-$place_bet), $message, $conn);
		} else {
			$message .= "The bet could not be inserted.";
			$message .= "<br />" . mysql_error()."<br />";
		}
	}
	
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Make bet for test purposes</h3>
				</div>
				<div class="panel-body">
				<?php $game=getGame(); ?>
					<form action=make_bet.php method="post">
						<?php 
							$current=0;
							if($game['current_round'] != -1){
								$current = $game['current_round'];
							}
							
// 							$matches_set=getMatchesWithClubNameByRoundAndSeason($current+1, $game['current_season']);
						?>
						<table class="table table-bordered">
							<tr>
								<th>S/R</th>
								<th>Home</th>
								<th>Away</th>
								<th>Bet</th>
								<th>1:X:2<br>1X:X2<br>H1:H2</th>
							</tr>
							<?php 
							
								for($i=1;$i<9;$i++){
									$match_SS=getMatchesWithAllInfomation($current+1, $game['current_season'], $i);
									//while - for home and away team information
									while ($match = mysql_fetch_array($match_SS)){
										if($match[0] == $match['home_club_id']){
											$homeTeam=$match;
										}else{
											$awayTeam=$match;
										}
									}
									
									echo "<tr>";
									echo "<td>{$homeTeam['season']}/{$homeTeam['round']}</td>";
									echo "<td>{$homeTeam['name']}</td>";
									echo "<td>{$awayTeam['name']}</td>";
								
									$coeff=getCoeff(getPercetangeForTeam($homeTeam, $awayTeam));
								
							?>
							
							<td><select name=bet<?php echo ($i-1); ?>>
									<option value="0">No bet</option>
									<option value="1:<?php echo $coeff[0]; ?>"><?php echo $homeTeam['name']; ?> to win</option>
									<option value="2:<?php echo $coeff[1]; ?>">Draw</option>
									<option value="3:<?php echo $coeff[2]; ?>"><?php echo $awayTeam['name']; ?> to win</option>
									<option value="4:<?php echo $coeff[3]; ?>"><?php echo $homeTeam['name']; ?> to win or draw</option>
									<option value="5:<?php echo $coeff[4]; ?>"><?php echo $awayTeam['name']; ?> to win or draw</option>
									<option value="6:<?php echo $coeff[5]; ?>"><?php echo $homeTeam['name']; ?> to win with H0:1</option>
									<option value="7:<?php echo $coeff[6]; ?>"><?php echo $awayTeam['name']; ?> to win with H1:0</option>
									
								</select>
							</td>
							<input type="hidden" value="<?php echo $homeTeam['id'];?>" name="match<?php echo ($i-1);?>">
							
							<?php 
								
									echo "<td>{$coeff[0]} : {$coeff[1]} : {$coeff[2]}
									<br>{$coeff[3]} : {$coeff[4]}
									<br>{$coeff[5]} : {$coeff[6]}</td>";

								}
							?>
						</table>
						<div class="input-group" style="margin-bottom: 20px; float:left">
							<span class="input-group-addon width_200">Bet:</span> <input
								type="text" class="form-control" placeholder="Bet"
								name="place_bet" maxlength="250" value="10" >
						</div>
						<div class="input-group" style="margin-bottom: 20px; margin-left:20px; float:left">
							<span class="input-group-addon width_200">User id:</span> <input
								type="text" class="form-control" placeholder="User id"
								name="user_id" maxlength="250" value="1" >
						</div>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
									value="Place bet" >
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>