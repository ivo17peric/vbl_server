<div class="header">
	<nav class="navbar navbar-default navbar-static-top no_bottom_margin" role="navigation">
	
		<div id="bs-example-navbar-collapse-1" style="margin-right: 20px; margin-left: 20px;">
		 	<ul class="nav navbar-nav navbar-right">
		 		<li style="line-height: 50px; vertical-align: middle; margin-left: 20px;">Login as:<?php echo " ".getUserName();?></li>
		 		<li style="line-height: 50px; vertical-align: middle; margin-left: 20px;">Email:<?php echo " ".getUserEmail();?></li>
		 		<li><a href="logout.php">Logout</a></li>
		 	</ul>
		</div>
		
	</nav>
</div>
