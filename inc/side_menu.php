<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php
if(!logged_in()){
	redirect_to("login.php");
}
?>
<ul class="nav" id="side-menu">
	<li><a href="add_new_user.php">Add new user</a></li>
	<li><a href="team_manage.php">Team manage</a></li>
	<li><a href="set_round.php">Set round</a></li>
	<li><a href="set_round_auto.php">Set round auto</a></li>
	<li><a href="current_round.php">Current round</a></li>
	<li><a href="custom_round.php">Custom round</a></li>
	<li><a href="table.php">League table</a></li>
	<li><a href="make_bet.php">Make bet (test)</a></li>
	<li><a href="list_bets.php">List all bets</a></li>
	<li><a href="list_users.php">List all users</a></li>
	<li><a href="reset_data.php">Reset data</a></li>
	<li><a href="logout.php">Logout</a></li>
	<li><a href="insert_users_custom.php?from=-1&to=-1">INSERT CUSTOM USERS</a></li>
	<li><a href="delete_all_users.php">DELTE ALL USERS</a></li>
	<li><a href="api/get_league_table.php?get=1">API - GET TABLE</a></li>
	<li><a href="api/get_round.php?get=1">API - CURRENT ROUND</a></li>
	<li><a href="api/get_round.php?get=1&offset=1">API - NEXT ROUND</a></li>
	<li><a href="api/get_round_for_bet.php?get=1&uuid=3">API - ROUND FOR BET</a></li>
	<li><a href="api/get_user_data.php?get=1&user_id=1">API - USER</a></li>
	<li><a href="api/get_user_data_with_pos.php?get=1&user_id=1">API - USER POS</a></li>
	<li><a href="api/get_user_bets.php?get=1&uuid=3">API - USER BETS</a></li>
	<li><a href="api/get_bet.php?get=1&bet_id=71">API - USER BET</a></li>
	<li><a href="api/get_master_data.php?get=1&user_id=1">API - MASTER</a></li>
	<li><a href="api/get_users.php?get=1&orderBy=p_points">API - USERS BY POINTS</a></li>
	<li><a href="api/create_user.php?get=1&uuid=-1&email=-1&deviceId=-1&username=-1">API - NEW USER</a></li>
	<li><a href="api/get_arround_user.php?get=1&user_id=1&orderBy=p_points">API - ARROUND USERS</a></li>
	<li><a href="api/get_users_and_around_users.php?get=1&orderBy=p_points&user_id=1">API - AROUND AND TOP</a></li>
</ul>