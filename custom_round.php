<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Custom round</h3>
				</div>
				<div class="panel-body">
				<?php $game=getGame(); ?>
					<form action=custom_round.php method="post">
						<?php 
							$isDone=1;
							$current=0;
							if($game['current_round'] != -1){
								$current = $game['current_round'];
							}
							
							if(isset($_POST['round'])){
								if($current<$_POST['round']) $isDone=0;
								$current = $_POST['round'];
							}
							
							if(isset($_POST['season'])){
								$season = $_POST['season'];
							}else{
								$season = $game['current_season'];
							}
							
							$matches_set=getMatchesWithClubNameByRoundAndSeason($current, $season);
						?>
						<table class="table table-bordered">
							<tr>
								<th>S/R</th>
								<th>Home</th>
								<th>Away</th>
								<th>Result</th>
								<th>Date</th>
							</tr>
							<?php 
								while ($match=mysql_fetch_array($matches_set)){
									echo "<tr>";
									echo "<td>{$match['season']}/{$match['round']}</td>";
									echo "<td>{$match['home']}</td>";
									echo "<td>{$match['away']}</td>";
									if($isDone){
										echo "<td>{$match['home_goals']} : {$match['away_goals']} </td>";
									}else{
										echo "<td>-:-</td>";
									}
									echo "<td>{$match['date_match']}</td>";
									echo "</tr>";
								}
							?>
						</table>
						<div class="input-group" style="margin-bottom: 20px; float:left">
							<span class="input-group-addon width_200">Round:</span> <input
								type="text" class="form-control" placeholder="Round"
								name="round" maxlength="250" value="<?php echo $current; ?>" >
						</div>
						<div class="input-group" style="margin-bottom: 20px; margin-left:20px; float:left">
							<span class="input-group-addon width_200">Season:</span> <input
								type="text" class="form-control" placeholder="Season"
								name="season" maxlength="250" value="<?php echo $season; ?>" >
						</div>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
									value="Show round" >
					</form>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>