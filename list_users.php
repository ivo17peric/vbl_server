<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/functions_for_bets.php"); ?>
<?php require_once("include/functions_for_results.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
$page=1;
$orderBy='id';
if(isset($_GET['page'])){
	$page=$_GET['page'];
	if($page<-1){
		$page=1;
	}
}
if(isset($_GET['orderBy'])){
	$orderBy = $_GET['orderBy'];
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 1200px">
				<div class="panel-heading">
					<h3 class="panel-title">List all users</h3>
				</div>
				<div class="panel-body">
					<ul class="pagination" style="float: left;margin: 0px; margin-bottom: 20px">
					  	<li><a href="list_users.php?orderBy=<?php echo $orderBy; ?>&page=<?php echo $page-1; ?>">&laquo;</a></li>
					  	<li class="active disabled"><a href="#"><?php echo $page; ?></a></li>
					  	<li><a href="list_users.php?orderBy=<?php echo $orderBy; ?>&page=<?php echo $page+1; ?>">&raquo;</a></li>
					</ul>
					<table class="table table-bordered">
						<tr>
							<th><a href="list_users.php?orderBy=id">Id</a></th>
							<th>UUID</th>
							<th>E-mail</th>
							<th>Username</th>
							<th><a href="list_users.php?orderBy=p_points">P_Points</a></th>
							<th><a href="list_users.php?orderBy=points">Points</a></th>
							<th><a href="list_users.php?orderBy=points_ten_games">10 G P</a></th>
							<th><a href="list_users.php?orderBy=points_ten_games_last">last 10 G P</a></th>
						</tr>
						<?php
							$users=getAllUsersBy(($page-1)*10, 10, $orderBy);
							
							while($row=mysql_fetch_array($users)){
								echo "<tr>";
								echo "<td>{$row['id']}</td>";
								echo "<td>{$row['uuid']}</td>";
								echo "<td>{$row['email']}</td>";
								echo "<td>{$row['username']}</td>";
								echo "<td>{$row['p_points']}</td>";
								echo "<td>{$row['points']}</td>";
								echo "<td>{$row['points_ten_games']}</td>";
								echo "<td>{$row['points_ten_games_last']}</td>";
								echo "</tr>";
							}
								
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>