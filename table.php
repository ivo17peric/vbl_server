<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
// if(!is_admin()){
// 	redirect_to("table.php");
// }
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Table</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th>P.</th>
							<th>Team</th>
							<th>GP</th>
							<th>WG</th>
							<th>DG</th>
							<th>LG</th>
							<th style="text-align:right;">GS</th>
							<th style="text-align:right;">GA</th>
							<th style="text-align:right;">GD</th>
							<th style="text-align:right;">Points</th>
							<th>Form</th>
							<th style="text-align:right;">OFF</th>
							<th style="text-align:right;">DEF</th>
						</tr>
						<?php
							$table=getTable();
							$i=1;
							while($row=mysql_fetch_array($table)){
								echo "<tr>";
								echo "<td>{$i}</td>";
								echo "<td>{$row['name']}</td>";
								echo "<td>{$row['games']}</td>";
								echo "<td>{$row['win_games']}</td>";
								echo "<td>{$row['draw_games']}</td>";
								echo "<td>{$row['lose_games']}</td>";
								echo "<td style=\"text-align:right;\">{$row['goals_scored']}</td>";
								echo "<td style=\"text-align:right;\">{$row['goals_allowed']}</td>";
								echo "<td style=\"text-align:right;\">".($row['goals_scored']-$row['goals_allowed'])."</td>";
								echo "<td style=\"text-align:right;font-weight:bold\">{$row['points']}</td>";
								echo "<td>";//{$row['last_five_form']}</td>";
								for($j=0;$j<5;$j++){
									if($row['last_five_form']{$j}==3){
										echo "<span style='color: #81D2FF'>D</span>";
									} else if($row['last_five_form']{$j}==1){
										echo "<span style='color: #9DF165'>O</span>";
									} else if($row['last_five_form']{$j}==2){
										echo "<span style='color: #FF7B85'>X</span>";
									}else {
										echo "-";
									}
								}
								echo "</td>";
								echo "<td style=\"text-align:right;\">{$row['offence']}</td>";
								echo "<td style=\"text-align:right;\">{$row['defence']}</td>";
								$i++;
								echo "<tr>";
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>