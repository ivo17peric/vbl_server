<?php require_once("include/functions.php") ?>
<?php
    session_start();
    
    $_SESSION=array();
    
    if(isset($_COOKIE[session_name()])){
        setcookie(session_name(), '',time()-42000,'/');
    }
    
    session_destroy();
    
    redirect_to("login.php");
?>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>