<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
include_once("include/form_function.php");

if (isset($_POST['submit'])){
	$errors = array();
	// perform validations on the form data
	$message="";
		
	$date_current=trim(mysql_prep($_POST['date_current']));
	$date_next=trim(mysql_prep($_POST['date_next']));
	$date_after_next=trim(mysql_prep($_POST['date_after_next']));
	
	$game=getGame();
	$current=0;
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}

	$query = "UPDATE game
		SET date_current='{$date_current}',
		date_next='{$date_next}',
		date_after_next='{$date_after_next}'
		WHERE id=1";
	$result = mysql_query($query, $conn);
	if ($result) {
		$message .= "Game was successfully updated.";
		
		for($i=1; $i<4; $i++){
			if($i==1){
				$date_SS=$date_current;
			}else if($i==2){
				$date_SS=$date_next;
			}else{
				$date_SS=$date_after_next;
			}
			$match = getMatchesByRoundAndSeason(($current+$i), $game['current_season']);
			if($match!=null ){
				$query = "UPDATE matches
					SET date_match='{$date_SS}'
					WHERE season={$match['season']} AND round={$match['round']}";
				$result = mysql_query($query, $conn);
				if ($result) {
					$message .= "<br />Matches in round {$match['round']} was successfully updated.";
				} else {
					$message .= "<br />Matches in round {$match['round']} could not be updated.";
					$message .= "<br />" . mysql_error();
				}
			}else{
				$round=$current+$i;
				for($j=1;$j<9;$j++){
					if($round<16){
						$ids=getClubIdFromSchedule($round, $j);
						$home_id[$j-1]=$ids['home'];
						$away_id[$j-1]=$ids['away'];
					}else{
						$ids=getClubIdFromSchedule($round-15, $j);
						$home_id[$j-1]=$ids['away'];
						$away_id[$j-1]=$ids['home'];
					}
				}
				
				$query = "INSERT INTO matches (season, round, match_cat, home_club_id, away_club_id, date_match) VALUES";
				
				for($j=1;$j<9;$j++){
					if($j==8){
						$query .="({$game['current_season']}, {$round}, {$j}, {$home_id[$j-1]}, {$away_id[$j-1]}, '{$date_SS}');";
					}else{
						$query .="({$game['current_season']}, {$round}, {$j}, {$home_id[$j-1]}, {$away_id[$j-1]}, '{$date_SS}'),";
					}
				}	
					
				$result = mysql_query($query, $conn);
				$mess_r=$current+$i;
				if ($result) {
					$message .= "<br />Round {$mess_r} was successfully created.";
				} else {
					$message .= "<br />Round {$mess_r} could not be created.";
					$message .= "<br />" . mysql_error();
				}
			}
		}
		
	} else {
		$message .= "Game could not be updated.";
		$message .= "<br />" . mysql_error();
	}
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Set round</h3>
				</div>
				<div class="panel-body">
				<?php $game=getGame(); ?>
					<form action="set_round.php" method="post">
						Current season: <?php echo $game['current_season']; ?><br />
						Current round: <?php echo $game['current_round']; ?><br />
						<?php 
							$current=0;
							if($game['current_round'] != -1){
								$current = $game['current_round'];
							}
						?>
						<table class="table table-bordered">
							<tr>
								<td>Round</td>
								<td>Date</td>
								<td>Hint</td>
							</tr>
							<tr>
								<td><?php echo $current+1; ?></td>
								<td><input type="text" class="form-control" 
												name="<?php echo "date_current" ;?>" maxlength="40" 
												value="<?php echo "{$game['date_current']}" ?>">
								</td>
								<td><i>(Format: dd/mm/yyyy HH:MM)</i></td>
							</tr>
							<tr>
								<td><?php echo $current+2; ?></td>
								<td><input type="text" class="form-control" 
												name="<?php echo "date_next" ;?>" maxlength="40" 
												value="<?php echo "{$game['date_next']}" ?>">
								</td>
								<td><i>(Format: dd/mm/yyyy HH:MM)</i></td>
							</tr>
							<tr>
								<td><?php echo $current+3; ?></td>
								<td><input type="text" class="form-control" 
												name="<?php echo "date_after_next" ;?>" maxlength="40" 
												value="<?php echo "{$game['date_after_next']}" ?>">
								</td>
								<td><i>(Format: dd/mm/yyyy HH:MM)</i></td>
							</tr>
						</table>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
							value="Set dates" >
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>