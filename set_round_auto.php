<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
include_once("include/form_function.php");

if (isset($_POST['submit'])){
	$errors = array();
	// perform validations on the form data
	$message="";
		
	$first_date=trim(mysql_prep($_POST['first_date']));
	
	//cehck if date is correct
	if(!validateDate($first_date)){
		echo "Date is not in format dd/mm/yyyy HH:MM!!";
		return;
	}
	
	if(!isDateInFuture($first_date)){
		echo "Date is in past!!";
		return;
	}
	
	$daysInterval = intval(trim(mysql_prep($_POST['days_interval'])));
	if(!is_int($daysInterval) || $daysInterval > 14 || $daysInterval < 1){
		echo "Days interval \"". $_POST['days_interval'] ."\" is not number or it is greater than 14 or lower than 1!!";
		return;
	}
	
	$roundCounter = intval(trim(mysql_prep($_POST['round_counter'])));
	
	$game=getGame();
	$current=0;
	if($game['current_round'] != -1){
		$current = $game['current_round'];
	}
	
	if(!is_int($roundCounter) || ($current + $roundCounter) > 30 || $roundCounter < 1){
		echo "Round counter \"". $_POST['round_counter'] ."\" is not number or it is over 30 round or lower than 1 round!!";
		return;
	}
	
	for ($i = 0; $i < $roundCounter; $i++){
		$date = DateTime::createFromFormat('d/m/Y H:i', $first_date);
		$date->modify('+'.($i * $daysInterval).' day');
		$date_temp = $date->format('d/m/Y H:i');
		
		if($i == 0){
			$query = "UPDATE game
			SET date_current='{$date_temp}'
			WHERE id=1";
			mysql_query($query, $conn);
		}else if($i == 1){
			$query = "UPDATE game
			SET date_next='{$date_temp}'
			WHERE id=1";
			mysql_query($query, $conn);
		}else if($i == 2){
			$query = "UPDATE game
			SET date_after_next='{$date_temp}'
			WHERE id=1";
			mysql_query($query, $conn);
		}
		
		$match = getMatchesByRoundAndSeason(($current+$i+1), $game['current_season']);
		if($match!=null ){
			$query = "UPDATE matches
			SET date_match='{$date_temp}'
			WHERE season={$match['season']} AND round={$match['round']}";
			$result = mysql_query($query, $conn);
			if ($result) {
				$message .= "<br />Matches in round {$match['round']} was successfully updated to date {$date_temp}.";
			} else {
				$message .= "<br />Matches in round {$match['round']} could not be updated.";
				$message .= "<br />" . mysql_error();
			}
		}else{
			$round=$current+$i+1;
			for($j=1;$j<9;$j++){
				if($round<16){
					$ids=getClubIdFromSchedule($round, $j);
					$home_id[$j-1]=$ids['home'];
					$away_id[$j-1]=$ids['away'];
				}else{
					$ids=getClubIdFromSchedule($round-15, $j);
					$home_id[$j-1]=$ids['away'];
					$away_id[$j-1]=$ids['home'];
				}
			}

			$query = "INSERT INTO matches (season, round, match_cat, home_club_id, away_club_id, date_match) VALUES";

			for($j=1;$j<9;$j++){
				if($j==8){
					$query .="({$game['current_season']}, {$round}, {$j}, {$home_id[$j-1]}, {$away_id[$j-1]}, '{$date_temp}');";
				}else{
					$query .="({$game['current_season']}, {$round}, {$j}, {$home_id[$j-1]}, {$away_id[$j-1]}, '{$date_temp}'),";
				}
			}

			$result = mysql_query($query, $conn);
			$mess_r=$current+$i+1;
			if ($result) {
				$message .= "<br />Round {$mess_r} was successfully created.";
			} else {
				$message .= "<br />Round {$mess_r} could not be created.";
				$message .= "<br />" . mysql_error();
			}
		}
	}
	
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Set round</h3>
				</div>
				<div class="panel-body">
				<?php $game=getGame(); ?>
					<form action="set_round_auto.php" method="post">
						Current season: <?php echo $game['current_season']; ?><br />
						Current round: <?php echo $game['current_round']; ?><br />
						<?php 
							$current=0;
							if($game['current_round'] != -1){
								$current = $game['current_round'];
							}
						?>
						<table class="table table-bordered">
							<tr>
								<td>Next Round</td>
								<td>Next Round Date <i>(Format: dd/mm/yyyy HH:MM)</i></td>
								<td>Days interval beetwen games</td>
								<td>Number of round</td>
							</tr>
							<tr>
								<td><?php echo $current+1; ?></td>
								<td><input type="text" class="form-control" 
												name="<?php echo "first_date" ;?>" maxlength="40" 
												value="" />
								</td>
								<td><input type="text" class="form-control" 
												name="<?php echo "days_interval" ;?>" maxlength="40" 
												value="" />
								</td>
								<td><input type="text" class="form-control" 
												name="<?php echo "round_counter" ;?>" maxlength="40" 
												value="" />
								</td>
							</tr>
						</table>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
							value="Set dates" >
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>