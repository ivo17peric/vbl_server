<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/functions_for_results.php"); ?>
<?php require_once("include/functions_for_updates_game.php"); ?>
<?php require_once("include/functions_for_bets.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
include_once("include/form_function.php");

if (isset($_POST['submit'])){
	
	$message="";
	
	$game=getGame();
	$current_round=$game['current_round'];
	if($current_round==-1){
		$current_round=1;
	}else{
		$current_round++;
	}
	
	if($game['date_current']==''){
		echo "First set date to round.<br/>";
		echo "<a href=\"current_round.php\">Back</a>";
		return;
	}
	
	//for - through all matches in defined round and currnet season 
	for($i=1;$i<9;$i++){
		$match_SS=getMatchesWithAllInfomation($current_round, $game['current_season'], $i);
		//while - for home and away team information
		while ($match = mysql_fetch_array($match_SS)){
			if($match[0] == $match['home_club_id']){
				$homeTeam=$match;
			}else{
				$awayTeam=$match;
			}
		}
		
		//echo "DOMACIN ".$homeTeam[0]." ".$homeTeam['name']."<br>";
		//echo "GOST ".$awayTeam[0]." ".$awayTeam['name']."<br>";
		$res=getPercetangeForTeam($homeTeam, $awayTeam);
		//echo $res[0]."% - ".$res[1]."% - ".$res[2]."%"."<br>";
		$coeff=getCoeff($res);
		//echo $coeff[0]." - ".$coeff[1]." - ".$coeff[2]."<br>";
		$result = getResult($res, $homeTeam, $awayTeam);
		//echo $result[0]." : ".$result[1];
		//echo "<hr>";
		
		//update match
		$message=updateMatchesTable($homeTeam['id'], $result, $message, $conn);
		
		//update table
		$message=updateLeagueTable($homeTeam[0], $awayTeam[0], $result, $message, $conn);
		
		//update form
		$message=updateForm($homeTeam, $awayTeam, $result, $message, $conn);
			
	}
	
	//update game
	$message=updateGameTables($current_round, $game, $message, $conn);
	$match = getMatchesByRoundAndSeason(($current_round+3), $game['current_season']);
	if($match!=null ){
		$message=updateGameTablesDateAfterNext($match['date_match'], $message, $conn);
	}
	
	//update bets and user points
	$message=updateBetsAndUser($game, $current_round, $message, $conn);
	
	//add 10 point to users
	$message=updateAllUsersPPoints($message, $conn);
	
}

?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">Current round</h3>
				</div>
				<div class="panel-body">
				<?php $game=getGame(); ?>
					<form action=current_round.php method="post">
						<?php 
							$current=0;
							if($game['current_round'] != -1){
								$current = $game['current_round'];
							}
							
							$matches_set=getMatchesWithClubNameByRoundAndSeason($current, $game['current_season']);
						?>
						<?php if ($current!=0){ ?>
							<table class="table table-bordered">
								<tr>
									<th>S/R</th>
									<th>Home</th>
									<th>Away</th>
									<th>Result</th>
									<th>Date</th>
								</tr>
								<?php 
									while ($match=mysql_fetch_array($matches_set)){
										echo "<tr>";
										echo "<td>{$match['season']}/{$match['round']}</td>";
										echo "<td>{$match['home']}</td>";
										echo "<td>{$match['away']}</td>";
										echo "<td>{$match['home_goals']} : {$match['away_goals']} </td>";
										echo "<td>{$match['date_match']}</td>";
										echo "</tr>";
									}
								?>
							</table>
						<?php }
							if($current+1<31){
								$matches_set=getMatchesWithClubNameByRoundAndSeason($current+1, $game['current_season']);
						?>
							<table class="table table-bordered">
								<tr>
									<th>S/R</th>
									<th>Home</th>
									<th>Away</th>
									<th>Result</th>
									<th>Date</th>
								</tr>
								<?php 
									while ($match=mysql_fetch_array($matches_set)){
										echo "<tr>";
										echo "<td>{$match['season']}/{$match['round']}</td>";
										echo "<td>{$match['home']}</td>";
										echo "<td>{$match['away']}</td>";
										echo "<td>-:-</td>";
										echo "<td>{$match['date_match']}</td>";
										echo "</tr>";
									}
								?>
							</table>
							<input class="btn btn-primary btn-block" type="submit" name="submit"
									value="Play <?php echo ($current+1)."."; ?> round" >
						<?php } ?>
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>