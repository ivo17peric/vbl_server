<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
include_once("include/form_function.php");

if (isset($_POST['submit'])){
	$errors = array();
	// perform validations on the form data
	$required_fields = array('username', 'password', 'email');
	$errors = array_merge($errors, check_required_fields($required_fields, $_POST));

	$field_with_lenght=$_POST['username'];
	$errors=array_merge($errors, check_max_field_lengths($field_with_lenght, 50));

	$field_with_lenght2=$_POST['password'];
	$errors=array_merge($errors, check_max_field_lengths($field_with_lenght2, 30));
	
	$field_with_lenght2=$_POST['email'];
	$errors=array_merge($errors, check_max_field_lengths($field_with_lenght2, 255));


	if($_POST['password']!=$_POST['password_2']){
		$nesto=array();
		$nesto[]="Passwords are not same.";
		$errors=array_merge($errors,$nesto);
	}

	$username = trim(mysql_prep($_POST['username']));
	$email = trim(mysql_prep($_POST['email']));
	$password = trim(mysql_prep($_POST['password']));
	$admin = trim(mysql_prep($_POST['admin']));
	$hashed_password = sha1($password);
	if ( empty($errors) ) {
		$query = "INSERT INTO php_users (
		username, hashed_password, admin, email
		) VALUES (
		'{$username}', '{$hashed_password}', $admin, '{$email}'
		)";
		$result = mysql_query($query, $conn);
		if ($result) {
			$message = "The user was successfully created.";
		} else {
			$message = "The user could not be created.";
			$message .= "<br />" . mysql_error();
		}
	} else {
		if (count($errors) == 1) {
			$message = "There was 1 error in the form.";
		} else {
			$message = "There were " . count($errors) . " errors in the form.";
		}
	}
}else {
	$username="";
	$password="";
	$admin="";
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id">
				<div class="panel-heading">
					<h3 class="panel-title">Create new user:</h3>
				</div>
				<div class="panel-body">
					<form action="add_new_user.php" method="post">
						<div class="input-group margin_bottom_width_915" style="width: 365px">
							<span class="input-group-addon width_200">Username:</span> <input
								type="text" class="form-control" placeholder="Username"
								name="username" maxlength="30" value="">
						</div>
						<div class="input-group margin_bottom_width_915" style="width: 365px">
							<span class="input-group-addon width_200">E-mail:</span> <input
								type="text" class="form-control" placeholder="Email"
								name="email" maxlength="20" value="">
						</div>
						<div class="input-group margin_bottom_width_915" style="width: 365px">
							<span class="input-group-addon width_200">Password:</span> <input
								type="password" class="form-control" placeholder="Password"
								name="password" maxlength="30" value="">
						</div>
						<div class="input-group margin_bottom_width_915" style="width: 365px">
							<span class="input-group-addon width_200">Repeat pass.:</span> <input
								type="password" class="form-control" placeholder="Password"
								name="password_2" maxlength="30" value="">
						</div>
						<div class="input-group margin_bottom_width_415" style="width: 365px">
							<span class="input-group-addon width_200">Admin:</span> 
							<select	name="admin" class="form-control">
								<option value="3">User</option>
								<option value="2">Moderator</option>
								<option value="1">Admin</option>
							</select>
						</div>
						<input class="btn btn-primary btn-block" type="submit" name="submit"
							value="Create user" >
					</form>
				</div>
			</div>
</div>
	</div>
	</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>