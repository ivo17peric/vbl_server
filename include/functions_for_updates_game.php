<?php
    
	function updateLeagueTable($home_id, $away_id, $result, $message, $conn){
		$points_home=0;
		$points_away=0;
		$win_home=0;
		$win_away=0;
		$draw_home=0;
		$draw_away=0;
		$lose_home=0;
		$lose_away=0;
		$goal_scored_home=$result[0];
		$goal_scored_away=$result[1];
		if($result[0]>$result[1]){
			$points_home=3;
			$win_home=1;
			$lose_away=1;
		}else if($result[0]==$result[1]){
			$draw_home=1;
			$draw_away=1;
			$points_home=1;
			$points_away=1;
		}else{
			$points_away=3;
			$win_away=1;
			$lose_home=1;
		}
		$query="UPDATE league_table
				SET games=games+1,
					points = CASE
						WHEN club_id={$home_id} THEN points + {$points_home}
						WHEN club_id={$away_id} THEN points + {$points_away}
					END,
					win_games = CASE
						WHEN club_id={$home_id} THEN win_games + {$win_home}
						WHEN club_id={$away_id} THEN win_games + {$win_away}
					END,
					draw_games = CASE
						WHEN club_id={$home_id} THEN draw_games + {$draw_home}
						WHEN club_id={$away_id} THEN draw_games + {$draw_away}
					END,
					lose_games = CASE
						WHEN club_id={$home_id} THEN lose_games + {$lose_home}
						WHEN club_id={$away_id} THEN lose_games + {$lose_away}
					END,
					goals_scored = CASE
						WHEN club_id={$home_id} THEN goals_scored + {$goal_scored_home}
						WHEN club_id={$away_id} THEN goals_scored + {$goal_scored_away}
					END,
					goals_allowed = CASE
						WHEN club_id={$home_id} THEN goals_allowed + {$goal_scored_away}
						WHEN club_id={$away_id} THEN goals_allowed + {$goal_scored_home}
					END
				WHERE club_id IN ({$home_id},{$away_id})";
		
		$result = mysql_query($query, $conn);
		if ($result) {
			$message .= "The table was successfully updated.<br />";
		} else {
			$message .= "The table could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		
		return $message;
	}
	
	function updateGameTables($current_round, $game, $message, $conn){
		$query="UPDATE game SET
					current_round={$current_round},
					date_current='{$game['date_next']}',
					date_next='{$game['date_after_next']}',
					date_after_next=''
				WHERE id=1;";
		$result = mysql_query($query, $conn);
		if ($result) {
			$message .= "The game was successfully updated.<br />";
		} else {
			$message .= "The game could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		
		return $message;
	}
	
	function updateGameTablesDateAfterNext($date, $message, $conn){
		$query="UPDATE game SET
			date_after_next='{$date}'
			WHERE id=1;";
		$result = mysql_query($query, $conn);
		if ($result) {
			$message .= "The game after next was successfully updated.<br />";
		} else {
			$message .= "The game could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}

		return $message;
	}
	
	function updateMatchesTable($match_id, $result, $message, $conn){
		$query="UPDATE matches SET
					home_goals={$result[0]},
					away_goals={$result[1]}
				WHERE
					id={$match_id}";
		$result = mysql_query($query, $conn);
		if ($result) {
			$message .= "The match with id {$match_id} was successfully updated.<br />";
		} else {
			$message .= "The match with id {$match_id} could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		
		return $message;
	}
	
	function updateForm($home, $away, $result, $message, $conn){
		$diffHome=$away['index_quality']-$home['index_quality'];
		$formChangeHome=0;
		$diffAway=$home['index_quality']-$away['index_quality'];
		$formChangeAway=0;
		$formHomeLast5=substr($home['last_five_form'], 0, -1);
		$formAwayLast5=substr($away['last_five_form'], 0, -1);
		
		if($result[0] == $result[1]){
			$formChangeHome=getFormDraw($diffHome);
			$formChangeAway=getFormDraw($diffAway);
			$formHomeLast5="3".$formHomeLast5;
			$formAwayLast5="3".$formAwayLast5;
		}else if($result[0] > $result[1]){
			$formChangeHome=getFormWin($diffHome, $result[0]-$result[1]);
			$formChangeAway=getFormLose($diffAway, $result[0]-$result[1]);
			$formHomeLast5="1".$formHomeLast5;
			$formAwayLast5="2".$formAwayLast5;
		}else{
			$formChangeAway=getFormWin($diffAway, $result[1]-$result[0]);
			$formChangeHome=getFormLose($diffHome, $result[1]-$result[0]);
			$formHomeLast5="2".$formHomeLast5;
			$formAwayLast5="1".$formAwayLast5;
		}
		
		if($home['form'] + $formChangeHome >= 10) {
			$formChangeHome = 10 - $home['form'];
		}
		if($home['form'] + $formChangeHome <= 0){
			$formChangeHome = 0 - $home['form'] + 0.1;
		}
		
		if($away['form'] + $formChangeAway >= 10) {
			$formChangeAway = 10 - $away['form'];
		}
		if($away['form'] + $formChangeAway <= 0){
			$formChangeAway = 0 - $away['form'] + 0.1;
		}
		
		$query="UPDATE clubs SET 
					form = CASE
						WHEN id={$home[0]} THEN form + {$formChangeHome}
						WHEN id={$away[0]} THEN form + {$formChangeAway}
					END,
					last_five_form = CASE
						WHEN id={$home[0]} THEN '{$formHomeLast5}'
						WHEN id={$away[0]} THEN '{$formAwayLast5}'
					END
				WHERE id IN ({$home[0]},{$away[0]})";
		
		$result = mysql_query($query, $conn);
		if ($result) {
			$message .= "The form with match id {$home['id']} was successfully updated.<br />";
		} else {
			$message .= "The form with match id {$home['id']} could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		
		return $message;
	}
	
	function getFormDraw($diff){
		if($diff>-11 && $diff<-5){
			return -0.3;
		}else if($diff<-10){
			return -1;
		}else if($diff>5 && $diff<11){
			return 0.3;
		}else if($diff>10){
			return 1;
		}
		return 0;
	}
	
	function getFormWin($diff, $diffGoal){
		if ($diff<6 && $diff>-6) {
			if($diffGoal>2){
				return 0.7;
			}
			return 0.5;
		}else if($diff>-11 && $diff<-5){
			if($diffGoal>2){
				return 0.4;
			}
			return 0.3;
		}else if($diff<-10){
			if($diffGoal>3){
				return 0.3;
			}
			return 0.2;
		}else if($diff>5 && $diff<11){
			if($diffGoal>1){
				return 0.7 + $diffGoal*0.2;
			}
			return 0.7;
		}else if($diff>10){
			if($diffGoal>1){
				return 1 + $diffGoal*0.3;
			}
			return 1;
		}
		return 0;
	}
	
	function getFormLose($diff, $diffGoal){
		if ($diff<6 && $diff>-6) {
			if($diffGoal>2){
				return -0.7;
			}
			return -0.5;
		}else if($diff>-11 && $diff<-5){
			if($diffGoal>1){
				return -0.7 - $diffGoal*0.2;
			}
			return -0.7;
		}else if($diff<-10){
			if($diffGoal>1){
				return -1 - $diffGoal*0.3;
			}
			return -1;
		}else if($diff>5 && $diff<11){
			if($diffGoal>2){
				return -0.4;
			}
			return -0.3;
		}else if($diff>10){
			if($diffGoal>3){
				return -0.3;
			}
			return -0.2;
		}
		return 0;
	}

?>