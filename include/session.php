<?php
    session_start();
    
    function logged_in(){
        return isset($_SESSION['user_id']);
    }
    
    function is_admin(){
    	if(logged_in()){
    		if($_SESSION['admin']==1){
    			return true;
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
    
    function is_moderator(){
    	if(logged_in()){
    		if($_SESSION['admin']==2){
    			return true;
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
    
    function is_basic_user(){
    	if(logged_in()){
    		if($_SESSION['admin']==3){
    			return true;
    		}else{
    			return false;
    		}
    	}else{
    		return false;
    	}
    }
    
    function confirm_logged_in(){
        if(!logged_in()){
            redirect_to("login.php");
        }
    }
    
    function getUserId(){
    	return $_SESSION['user_id'];
    }
    
    function getUserName(){
    	return $_SESSION['username'];
    }
    
    function getUserEmail(){
    	return $_SESSION['email'];
    }

?>