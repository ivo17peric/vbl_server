<?php
    
	function isHomeBetter($home_IQ, $away_IQ){
		return $home_IQ >= $away_IQ;
	}
	
	function qualityDifferent($better, $worse){
		return ($better-$worse);
	}
	
	function getBetterWinPercentage($better, $worse, $IQ_diff){
		$win=35;
		if($IQ_diff > 15){
			$win=35 +(5*2.4) + (5*2.6) + (5*2.9) + (($IQ_diff-15)*3.3); 
		}else if($IQ_diff > 10){
			$win=35 +(5*2.4) + (5*2.6) + (($IQ_diff-10)*2.9);
		}else if($IQ_diff > 5){
			$win=35 +(5*2.4) + (($IQ_diff-5)*2.6);
		}else{
			$win=35 +(($IQ_diff)*2.4);
		}
		return $win;
	}
	
	function getBetterDrawPercentage($better, $worse, $IQ_diff){
		$draw=30;
		if($IQ_diff > 15){
			$draw=30 - (5*0.7) - (5*0.9) - (5*1.2) - (($IQ_diff-15)*1.8);
		}else if($IQ_diff > 10){
			$draw=30 - (5*0.7) - (5*0.9) - (($IQ_diff-10)*1.2);
		}else if($IQ_diff > 5){
			$draw=30 - (5*0.7) - (($IQ_diff-5)*0.9);
		}else{
			$draw=30 -(($IQ_diff)*0.7);
		}
		return $draw;
	}
	
	function getBetterLosePercentage($win, $draw){
		return 100-$draw-$win;
	}
	
	function reformatWithFormHomeBetter($res, $diff_form){
		$win=$res[0]+($res[0]*($diff_form*5)/100);
		$lose=$res[2]-($res[2]*($diff_form*5)/100);
		$draw=100-$win-$lose;
		return array($win, $draw, $lose);
	}
	
	function reformatWithFormAwayBetter($res, $diff_form){
		$win=$res[0]+($res[0]*($diff_form*5)/100);
		$lose=$res[2]-($res[2]*($diff_form*5)/100);
		$draw=100-$win-$lose;
		return array($lose, $draw, $win);
	}
	
	function getPercetangeForTeam($home, $away){
		if(isHomeBetter($home['index_quality'], $away['index_quality'])){
			$diff=qualityDifferent($home['index_quality'], $away['index_quality']);
			$win=getBetterWinPercentage($home, $away, $diff);
			$draw=getBetterDrawPercentage($home, $away, $diff);
			$lose=getBetterLosePercentage($win, $draw);
			return reformatWithFormHomeBetter(array($win, $draw, $lose), $home['form']-$away['form']);
		}else{
			$diff=qualityDifferent($away['index_quality'], $home['index_quality']);
			$win=getBetterWinPercentage($away, $home, $diff);
			$draw=getBetterDrawPercentage($away, $home, $diff);
			$lose=getBetterLosePercentage($win, $draw);
			return reformatWithFormAwayBetter(array($win, $draw, $lose), $away['form']-$home['form']);
		}
	}
	
	function getCoeff($res){
		$home_win=round(100/$res[0], 2);
		$draw=round(100/$res[1], 2);
		$away_win=round(100/$res[2], 2);
		$home_win_or_draw=round($home_win / (1.45 + (ceil($home_win-2) * 0.25)), 2);
		if ($home_win_or_draw < 1.01) $home_win_or_draw =1.01;
		
		$away_win_or_draw=round($away_win / (1.45 + (ceil($away_win-2) * 0.25)), 2);
		if ($away_win_or_draw < 1.01) $away_win_or_draw =1.01;
		
		$home_win_hendicep=round($home_win * (1.6 + (ceil($home_win-2) * 0.3)), 2);
		$away_win_hendicep=round($away_win * (1.6 + (ceil($away_win-2) * 0.3)), 2);
		return array($home_win, $draw, $away_win, $home_win_or_draw, $away_win_or_draw, $home_win_hendicep, $away_win_hendicep);
	}
	
	function getResultWinner($res){
		$rnd=rand(1, 100);
		//echo $rnd." | ";
		if($rnd > ($res[0] + $res[1])){
			return 3; 
		}else if($rnd > $res[0]){
			return 2;
		}else{
			return 1;
		}
	}
	
	function getResult($res, $home, $away){
		$arrayGoals=array(
						0 => 11,
						1 => 23,
						2 => 35,
						3 => 19,
						4 => 8,
						5 => 3,
						6 => 1);
		
		$getResult=getResultWinner($res);
// 		echo $getResult." | ";
		if($getResult==2){
			$TM1_index = $home['offence']-$home['defence'];
			$TM2_index = $away['offence']-$away['defence'];
			$index=($TM1_index+$TM2_index)/10;
			
			$arrayGoals[0]=11-($index*11/100);
			$arrayGoals[1]=23-($index*23/100);
			$arrayGoals[2]=35-($index*35/100);
			
			$newDiff=11+23+35-$arrayGoals[0]-$arrayGoals[1]-$arrayGoals[2];
			
			$arrayGoals[5]=3+$newDiff/3;
			if($arrayGoals[5]<0){
				$arrayGoals[5]=0;
			}
			$arrayGoals[6]=1+$newDiff/3;
			if($arrayGoals[6]<0){
				$arrayGoals[6]=0;
			}
			$arrayGoals[4]=100-($arrayGoals[0]+$arrayGoals[1]+$arrayGoals[2]+$arrayGoals[3]+$arrayGoals[5]+$arrayGoals[6]);
			if($arrayGoals[4]<0){
				$arrayGoals[4]=0;
			}
			
			$rnd=rand(1, 100);
// 			echo "<br><br><br>";
// 			print_r($arrayGoals);
// 			echo $rnd." | ";
			if($rnd > ($arrayGoals[0]+$arrayGoals[1]+$arrayGoals[2]+$arrayGoals[3]+$arrayGoals[4]+$arrayGoals[5])){
				return array(6,6);
			}else if($rnd > $arrayGoals[0]+$arrayGoals[1]+$arrayGoals[2]+$arrayGoals[3]+$arrayGoals[4]){
				return array(5,5);
			}else if($rnd > $arrayGoals[0]+$arrayGoals[1]+$arrayGoals[2]+$arrayGoals[3]){
				return array(4,4);
			}else if($rnd > $arrayGoals[0]+$arrayGoals[1]+$arrayGoals[2]){
				return array(3,3);
			}else if($rnd > $arrayGoals[0]+$arrayGoals[1]){
				return array(2,2);
			}else if($rnd > $arrayGoals[0]){
				return array(1,1);;
			}else{
				return array(0,0);
			}
		}else {
			if($getResult==1){
				$index=($home['offence']-$away['defence'])/10;
			}else{
				$index=($away['offence']-$home['defence'])/10;
			}
			$tempArray=$arrayGoals;
			
			$tempArray[0]=0;
			$tempArray[1]=23-($index*23/100);
			$tempArray[2]=35-($index*35/100);
			
			$newDiff=23+35-$tempArray[1]-$tempArray[2];
			
			$tempArray[5]=3+$newDiff/3;
			if($tempArray[5]<0){
				$tempArray[5]=0;
			}
			$tempArray[6]=1+$newDiff/3;
			if($tempArray[6]<0){
				$tempArray[6]=0;
			}
			$tempArray[4]=100-($tempArray[0]+$tempArray[1]+$tempArray[2]+$tempArray[3]+$tempArray[5]+$tempArray[6]);
			if($tempArray[4]<0){
				$tempArray[4]=0;
			}
// 			echo "<br><br><br>";
// 			print_r($tempArray);
			$rnd=rand(1, 100);
// 			echo $rnd." | ";
			if($rnd > ($tempArray[1]+$tempArray[2]+$tempArray[3]+$tempArray[4]+$tempArray[5])){
				$winScore=6;
			}else if($rnd > $tempArray[1]+$tempArray[2]+$tempArray[3]+$tempArray[4]){
				$winScore=5;
			}else if($rnd > $tempArray[1]+$tempArray[2]+$tempArray[3]){
				$winScore=4;
			}else if($rnd > $tempArray[1]+$tempArray[2]){
				$winScore=3;
			}else if($rnd > $tempArray[1]){
				$winScore=2;
			}else {
				$winScore=1;
			}
			
			if($getResult==1){
				$index=($away['offence']-$home['defence'])/10;
			}else{
				$index=($home['offence']-$away['defence'])/10;
			}
			$tempArray=$arrayGoals;
				
			$tempArray[0]=11-($index*11/100);
			$tempArray[1]=23-($index*23/100);
			$tempArray[2]=35-($index*35/100);
				
			$newDiff=11+23+35-$tempArray[0]-$tempArray[1]-$tempArray[2];
				
			$tempArray[5]=3+$newDiff/3;
			if($tempArray[5]<0){
				$tempArray[5]=0;
			}
			$tempArray[6]=1+$newDiff/3;
			if($tempArray[6]<0){
				$tempArray[6]=0;
			}
			$tempArray[4]=100-($tempArray[0]+$tempArray[1]+$tempArray[2]+$tempArray[3]+$tempArray[5]+$tempArray[6]);
			if($tempArray[4]<0){
				$tempArray[4]=0;
			}
			$max=100;
			for($i=6;$i>=$winScore;$i--){
				$max=$max-$tempArray[$i];
			}
// 			echo "<br><br><br>";
// 			print_r($tempArray);
			$rnd=rand(1, $max);
// 			echo $rnd." | ";
			if($rnd > $tempArray[0]+$tempArray[1]+$tempArray[2]+$tempArray[3]+$tempArray[4]){
				$loseScore=5;
			}else if($rnd > $tempArray[0]+$tempArray[1]+$tempArray[2]+$tempArray[3]){
				$loseScore=4;
			}else if($rnd > $tempArray[0]+$tempArray[1]+$tempArray[2]){
				$loseScore=3;
			}else if($rnd > $tempArray[0]+$tempArray[1]){
				$loseScore=2;
			}else if($rnd > $tempArray[0]){
				$loseScore=1;
			}else {
				$loseScore=0;
			}
			
			if($getResult==1){
				return array($winScore, $loseScore);
			}else{
				return array($loseScore, $winScore);
			}
		}
	}

?>