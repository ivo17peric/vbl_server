<?php
    
    function mysql_prep ($value){
        $magic_qouotes_active = get_magic_quotes_gpc();
        $new_enough_php = function_exists("mysql_real_escape_string");
        if ($new_enough_php){
            if ($magic_qouotes_active){$value=stripslashes($value);}
            $value=mysql_real_escape_string($value);
        } else {
            if(!$magic_qouotes_active){$value=addslashes($value);}
        }
        return $value;
    }
    function redirect_to($location){
        if($location){
            header("Location: {$location}");
            exit;
        }
    }
    function redirect_to_after($location, $seconds){
        if($location){
            sleep($seconds);
            header("Location: {$location}");
            exit;
        }
    }
    function confirm_query($result_set){
        if(!$result_set){
            die("Database query failed: " . mysql_error());
        }
    }
   
	function getUserPhp(){
    	global $conn;
    	$query="select * from php_users";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return $users;
    }
    
    function getClubs(){
    	global $conn;
    	$query="select * from clubs";
    	$clubs = mysql_query($query, $conn);
    	confirm_query($clubs);
    	return $clubs;
    }
    
    function getGame(){
    	global $conn;
    	$query="select * from game limit 1";
    	$game = mysql_query($query, $conn);
    	confirm_query($game);
    	return mysql_fetch_array($game);
    }
    
    function getMatchesByRoundAndSeason($round, $season){
    	global $conn;
    	$query="select * from matches WHERE round={$round} AND season={$season} limit 1";
    	$match = mysql_query($query, $conn);
    	confirm_query($match);
    	return mysql_fetch_array($match);
    }
    
    function getAllMatchesByRoundAndSeason($round, $season){
    	global $conn;
    	$query="select * from matches WHERE round={$round} AND season={$season}";
    	$match = mysql_query($query, $conn);
    	confirm_query($match);
    	return $match;
    }
    
    function getClubIdFromSchedule($round, $match_SS){
    	global $conn;
    	$query="SELECT
    				t1.id AS home,
    				t2.id AS away
   				FROM
    				clubs AS t1,
    				clubs AS t2
    			WHERE
    				t1.id_schedule = (SELECT LEFT(schedule.match_{$match_SS},1) FROM schedule WHERE round={$round})
            	AND
    				t2.id_schedule = (SELECT RIGHT(schedule.match_{$match_SS},1) FROM schedule WHERE round={$round})";
//     	$query="SELECT clubs.id FROM `clubs` WHERE clubs.id_schedule=
//     			(SELECT {$leftOrRight}(schedule.match_{$match_SS}, 1) FROM `schedule` WHERE round={$round})";
    	$id = mysql_query($query, $conn);
    	confirm_query($id);
    	$id_set=mysql_fetch_array($id);
    	return $id_set;
    }
    
    function getMatchesWithClubNameByRoundAndSeason($round, $season){
    	global $conn;
    	$query="SELECT
    			t1.name AS home,
    			t1.offence AS home_offence,
    			t1.defence AS home_defence,
    			t1.last_five_form AS home_last_form,
    			t2.name AS away,
    			t2.offence AS away_offence,
    			t2.defence AS away_defence,
    			t2.last_five_form AS away_last_form,
    			matches.id,
    			matches.round,
    			matches.season,
    			matches.home_goals,
   				matches.away_goals,
    			matches.date_match,
    			matches.round,
    			matches.season
   			 FROM
    			matches,
    			clubs AS t1,
    			clubs AS t2
    		WHERE
    			matches.home_club_id = t1.id
    		AND
    			matches.away_club_id = t2.id
    		AND
    			matches.round={$round}
    		AND
    			matches.season = {$season}
    		ORDER BY
    			matches.match_cat";
    	
    	$matches = mysql_query($query, $conn);
    	confirm_query($matches);
    	return $matches;
    }
    
    function getMatchesWithAllInfomation($round, $season, $cat){
    	global $conn;
    	$query="SELECT * FROM clubs, matches 
				WHERE 
					(matches.home_club_id = clubs.id AND round={$round} AND season={$season} AND match_cat={$cat}) 
				OR 
					(matches.away_club_id = clubs.id AND round={$round} AND season={$season} AND match_cat={$cat})";
    	$matches = mysql_query($query, $conn);
    	confirm_query($matches);
    	return $matches;
    }
    
    function getAllBetsByRoundAndSeason($round, $season){
    	global $conn;
    	$query="select * from bets WHERE round={$round} AND season={$season}";
    	$match = mysql_query($query, $conn);
    	confirm_query($match);
    	return $match;
    }
    
    function getAllBets($offset, $count){
    	global $conn;
    	$query="SELECT * FROM bets ORDER BY id DESC LIMIT {$offset}, {$count}";
    	$bets = mysql_query($query, $conn);
    	confirm_query($bets);
    	return $bets;
    }
    
    function getAllBetsByUserId($offset, $count, $userId){
    	global $conn;
    	$query="SELECT * FROM bets WHERE user_id = {$userId} ORDER BY id DESC LIMIT {$offset}, {$count}";
    	$bets = mysql_query($query, $conn);
    	confirm_query($bets);
    	return $bets;
    }
    
    function getNumberOffAllBetsByUserId($userId){
    	global $conn;
    	$query="SELECT * FROM bets WHERE user_id = {$userId}";
    	$bets = mysql_query($query, $conn);
    	confirm_query($bets);
    	return mysql_num_rows($bets);
    }
    
    function getBetById($id){
    	global $conn;
    	$query="SELECT * FROM bets WHERE id={$id}";
    	$bet = mysql_query($query, $conn);
    	confirm_query($bet);
    	return mysql_fetch_array($bet);
    }
    
    function getJustClubNamesFromMatch($id){
    	global $conn;
    	$query="SELECT 
				t1.name AS home, 
		    	t2.name AS away 
			FROM 
		    	matches, 
		    	clubs AS t1, 
		        clubs AS t2
		    WHERE 
		    	matches.home_club_id=t1.id 
		    AND 
		    	matches.away_club_id=t2.id 
		    AND 
		    	matches.id={$id}";
    	$names = mysql_query($query, $conn);
    	confirm_query($names);
    	return mysql_fetch_array($names);
    }
    
    function getTable(){
    	global  $conn;
    	$query="SELECT * FROM league_table, clubs 
    			WHERE league_table.club_id = clubs.id
				ORDER BY 
    				points DESC, 
    				(goals_scored-goals_allowed) DESC, 
    				goals_scored DESC, 
    				league_table.id ASC";
    	$table = mysql_query($query, $conn);
    	confirm_query($table);
    	return $table;
    }
    
    function getUserById($id){
    	global  $conn;
    	$query="SELECT * FROM users WHERE id={$id}";
    	$user = mysql_query($query, $conn);
    	confirm_query($user);
    	return mysql_fetch_array($user, MYSQL_ASSOC);
    }
    
    function getUserByUuid($uuid){
    	global  $conn;
    	$query="SELECT * FROM users WHERE uuid='{$uuid}'";
    	$user = mysql_query($query, $conn);
    	confirm_query($user);
    	return mysql_fetch_array($user, MYSQL_ASSOC);
    }
    
    function getAllUsers($offset, $count){
    	global  $conn;
    	$query="SELECT * FROM users ORDER BY id DESC LIMIT {$offset}, {$count}";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return $users;
    }
    
    function getAllUsersBy($offset, $count, $order){
    	global  $conn;
    	$query="SELECT * FROM users ORDER BY {$order} DESC LIMIT {$offset}, {$count}";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return $users;
    }
    
    function getAllUsersBelowBy($offset, $count, $order, $id, $score){
    	global  $conn;
    	$query="SELECT * FROM users WHERE {$order} <= {$score} AND id != {$id} ORDER BY {$order} DESC LIMIT {$offset}, {$count}";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return $users;
    }
    
    function getAllUsersAboveBy($offset, $count, $order, $id, $score){
    	global  $conn;
    	$query="SELECT * FROM users WHERE {$order} > {$score} ORDER BY {$order} ASC LIMIT {$offset}, {$count}";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return $users;
    }
    
    function getNumberOfUsersAboveBy($order, $id, $score){
    	global  $conn;
    	$query="SELECT * FROM users WHERE {$order} > {$score} ORDER BY {$order} ASC";
    	$users = mysql_query($query, $conn);
    	confirm_query($users);
    	return mysql_num_rows($users);
    }
    
    function createJsonResponse($data, $dataKey, $isSucces, $message){
    	$response = array('result' => $isSucces, 'message' => $message, $dataKey => $data);
    	return json_encode($response);
    }
    
    function reoderArray($array){
    	$newArray = array();
    	$j=sizeof($array);
    	for($i = 0; $i < $j; $i++){
    		$newArray[$i] = $array[$j - $i - 1];
    	}
    	return $newArray;
    }
    
    function checkIfUserIsTakken($username){
    	global  $conn;
    	$query="SELECT * FROM users WHERE username = '{$username}'";
    	$user = mysql_query($query, $conn);
    	confirm_query($user);
    	if(mysql_num_rows($user) > 0){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    function getExportUserByIdAndPass($id, $password){
    	global  $conn;
    	$pass = sha1($password);
    	$query="SELECT * FROM export WHERE user_id=$id AND password='{$pass}' ORDER BY id DESC LIMIT 1";
    	$user = mysql_query($query, $conn);
    	confirm_query($user);
    	return mysql_fetch_array($user, MYSQL_ASSOC);
    }
    
    function validateDate($date)
    {
    	$d = DateTime::createFromFormat('d/m/Y H:i', $date); //dd/mm/yyyy HH:MM //Y-m-d H:i:s
    	return $d && $d->format('d/m/Y H:i') == $date;
    }
    
    function isDateInFuture($date)
    {
    	$d = DateTime::createFromFormat('d/m/Y H:i', $date); //dd/mm/yyyy HH:MM //Y-m-d H:i:s
    	$n = DateTime::createFromFormat('d/m/Y H:i', date('d/m/Y H:i'));
    	return $d > $n;
    }
    
    function convert_number_to_words($number) {
    
    	$hyphen      = '-';
    	$conjunction = ' and ';
    	$separator   = ', ';
    	$negative    = 'negative ';
    	$decimal     = ' point ';
    	$dictionary  = array(
    			0                   => 'zero',
    			1                   => 'one',
    			2                   => 'two',
    			3                   => 'three',
    			4                   => 'four',
    			5                   => 'five',
    			6                   => 'six',
    			7                   => 'seven',
    			8                   => 'eight',
    			9                   => 'nine',
    			10                  => 'ten',
    			11                  => 'eleven',
    			12                  => 'twelve',
    			13                  => 'thirteen',
    			14                  => 'fourteen',
    			15                  => 'fifteen',
    			16                  => 'sixteen',
    			17                  => 'seventeen',
    			18                  => 'eighteen',
    			19                  => 'nineteen',
    			20                  => 'twenty',
    			30                  => 'thirty',
    			40                  => 'fourty',
    			50                  => 'fifty',
    			60                  => 'sixty',
    			70                  => 'seventy',
    			80                  => 'eighty',
    			90                  => 'ninety',
    			100                 => 'hundred',
    			1000                => 'thousand',
    			1000000             => 'million',
    			1000000000          => 'billion',
    			1000000000000       => 'trillion',
    			1000000000000000    => 'quadrillion',
    			1000000000000000000 => 'quintillion'
    	);
    
    	if (!is_numeric($number)) {
    		return false;
    	}
    
    	if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
    		// overflow
    		trigger_error(
    		'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
    		E_USER_WARNING
    		);
    		return false;
    	}
    
    	if ($number < 0) {
    		return $negative . convert_number_to_words(abs($number));
    	}
    
    	$string = $fraction = null;
    
    	if (strpos($number, '.') !== false) {
    		list($number, $fraction) = explode('.', $number);
    	}
    
    	switch (true) {
    		case $number < 21:
    			$string = $dictionary[$number];
    			break;
    		case $number < 100:
    			$tens   = ((int) ($number / 10)) * 10;
    			$units  = $number % 10;
    			$string = $dictionary[$tens];
    			if ($units) {
    				$string .= $hyphen . $dictionary[$units];
    			}
    			break;
    		case $number < 1000:
    			$hundreds  = $number / 100;
    			$remainder = $number % 100;
    			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
    			if ($remainder) {
    				$string .= $conjunction . convert_number_to_words($remainder);
    			}
    			break;
    		default:
    			$baseUnit = pow(1000, floor(log($number, 1000)));
    			$numBaseUnits = (int) ($number / $baseUnit);
    			$remainder = $number % $baseUnit;
    			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
    			if ($remainder) {
    				$string .= $remainder < 100 ? $conjunction : $separator;
    				$string .= convert_number_to_words($remainder);
    			}
    			break;
    	}
    
    	if (null !== $fraction && is_numeric($fraction)) {
    		$string .= $decimal;
    		$words = array();
    		foreach (str_split((string) $fraction) as $number) {
    			$words[] = $dictionary[$number];
    		}
    		$string .= implode(' ', $words);
    	}
    
    	return ucfirst($string);
    }
    
?>