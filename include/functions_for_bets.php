<?php
    
	function parseBets($bets){
// 		$bets = "123?1;144?2;144?3;122?2;111?1";
		$parts = explode(';', $bets);
		$betsArray=array();
		foreach ($parts as $part_SS){
			$pp = explode("?", $part_SS);
			array_push($betsArray, array('id' => $pp[0], 'bet' => $pp[1]));
		}
		return $betsArray;
	}
	
	function insertResultOfBet($res, $betId, $message, $conn){
		$query="UPDATE bets SET 
				result = {$res}
				WHERE id = {$betId}";
		$result=mysql_query($query, $conn);
		
		if ($result) {
			$message .= "The bets with id {$betId} was successfully updated.<br />";
		} else {
			$message .= "The bets with id {$betId} could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		return $message;
	}
	
	function updateUsersPoints($userId, $points, $round, $message, $conn){
		$query="UPDATE users SET
				p_points=p_points+{$points},
				points=points+{$points}";
		if($round%10==1){
			$query.=", points_ten_games_last=points_ten_games,
					points_ten_games={$points}";
		}else{
			$query.=", points_ten_games=points_ten_games+{$points}";
		}
		$query.=" WHERE id={$userId}";
		$result=mysql_query($query, $conn);
		
		if ($result) {
			$message .= "The user with id {$userId} was successfully updated.<br />";
		} else {
			$message .= "The user with id {$userId} could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		return $message;
	}
	
	function updateUsersPPoints($userId, $points, $message, $conn){
		$query="UPDATE users SET
					p_points=p_points+{$points}
					WHERE id={$userId}";
		$result=mysql_query($query, $conn);
	
		if ($result) {
			$message .= "The user with id {$userId} was successfully updated.<br />";
		} else {
			$message .= "The user with id {$userId} could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		return $message;
	}
	
	function updateAllUsersPPoints($message, $conn){
		$query="UPDATE users SET
				p_points=p_points+10";
		
		$result=mysql_query($query, $conn);
	
		if ($result) {
			$message .= "The all users were successfully updated.<br />";
		} else {
			$message .= "The all users could not be updated.";
			$message .= "<br />" . mysql_error()."<br />";
		}
		return $message;
	}
	
	function updateBetsAndUser($game, $current_round, $message, $conn){
		$matches_SS=getAllMatchesByRoundAndSeason($current_round, $game['current_season']);
		$matches=array();
		while ($row=mysql_fetch_array($matches_SS)){
			$matches["{$row['id']}"]=$row;
		}
		
		$temp=getAllBetsByRoundAndSeason($current_round, $game['current_season']);
		while($row=mysql_fetch_array($temp)){
			$bets=parseBets($row['bet']);
			$res=1;
			$count=0;
			$koefM=0;
			foreach ($bets as $bet){
				$count++;
				if($matches["{$bet['id']}"]['home_goals']>$matches["{$bet['id']}"]['away_goals']){
					if($bet['bet'] == 1 || $bet['bet'] == 4){
						//WIN
					}else if($bet['bet'] == 6 && $matches["{$bet['id']}"]['home_goals']>1+$matches["{$bet['id']}"]['away_goals']){
						//WIN
					}else{
						//LOSE
						if($res==3) $res=2;
						if($res==1) $res=3;
					}
				}else if($matches["{$bet['id']}"]['home_goals']<$matches["{$bet['id']}"]['away_goals']){
					if($bet['bet'] == 3 || $bet['bet'] == 5){
						//WIN
					}else if($bet['bet'] == 7 && $matches["{$bet['id']}"]['home_goals']+1<$matches["{$bet['id']}"]['away_goals']){
						//WIN
					}else{
						//LOSE
						if($res==3) $res=2;
						if($res==1) $res=3;
					}
				}else{
					if($bet['bet'] == 2 || $bet['bet'] == 4 || $bet['bet'] == 5){
						//WIN
					}else{
						//LOSE
						if($res==3) $res=2;
						if($res==1) $res=3;
					}
				}
			}
			
			$points=0;
			if($res==1) $points=$row['koef']*$row['bet_place'];
			if($res==3 && $count>4) $points=$row['koef']*$row['bet_place']/$row['max_koef'];
			if($res==3 && $count<5) $res=2;
			$message=insertResultOfBet($res, $row['id'], $message, $conn);
			$message=updateUsersPoints($row['user_id'], $points, $current_round, $message, $conn);
		}
		
		return $message;
	}

?>