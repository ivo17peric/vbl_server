<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php require_once("include/functions_for_bets.php"); ?>
<?php require_once("include/functions_for_results.php"); ?>
<?php  confirm_logged_in();
if(!is_admin()){
	redirect_to("table.php");
}
?>
<?php
$page=1;
if(isset($_GET['page'])){
	$page=$_GET['page'];
	if($page<-1){
		$page=1;
	}
}
?>
<html>
	<head>
		<?php
			$title_in_head="Admin";
			require("inc/head_init.php");
		?>
	</head>
	<body>
	<div id="wrapper">
		<?php
			require("inc/header_in_wrapper.php");
			require("inc/side_menu_wrapper.php");
		?>
		<div id="center" style="width: 400px;">
		<?php
			if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
			<?php
			if(!empty($errors)){
				display_errors($errors);
			}
		?>
		<div class="panel panel-primary panel_main" id="float_left_id" style="width: 900px">
				<div class="panel-heading">
					<h3 class="panel-title">List all bets</h3>
				</div>
				<div class="panel-body">
					<ul class="pagination" style="float: left;margin: 0px; margin-bottom: 20px">
					  	<li><a href="list_bets.php?page=<?php echo $page-1; ?>">&laquo;</a></li>
					  	<li class="active disabled"><a href="#"><?php echo $page; ?></a></li>
					  	<li><a href="list_bets.php?page=<?php echo $page+1; ?>">&raquo;</a></li>
					</ul>
					<table class="table table-bordered">
						<tr>
							<th>Id</th>
							<th>S/R</th>
							<th>Bet</th>
							<th>User Id</th>
							<th>Coeff</th>
							<th>Max Coeff</th>
							<th>Result</th>
							<th></th>
						</tr>
						<?php
							$bets_set=getAllBets(($page-1)*10, 10);
							
							while($bets=mysql_fetch_array($bets_set)){
								echo "<tr>";
								echo "<td>{$bets['id']}</td>";
								echo "<td>{$bets['season']}/{$bets['round']}</td>";
								echo "<td>{$bets['bet_place']}</td>";
								echo "<td>{$bets['user_id']}</td>";
								echo "<td>{$bets['koef']}</td>";
								echo "<td>{$bets['max_koef']}</td>";
								if($bets['result']==1){
										echo "<td style=\"color:green\">WIN => ".round(($bets['bet_place']*$bets['koef']),2)."</td>";
								}else if($bets['result']==3){
									echo "<td style=\"color:lightblue\">HALF WIN =>" .round(($bets['bet_place']*$bets['koef']/$bets['max_koef']),2)."</td>";
								}else if($bets['result']==2){
									echo "<td style=\"color:red\">LOST</td>";
								}else {
									echo "<td>WAIT</td>";
								}
								echo "<td><a class=\"btn btn-default btn-block btn-xs\" href=\"single_bet.php?id={$bets['id']}\">ENTER</a></td>";
								echo "</tr>";
							}
								
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<?php
if(isset($conn)){
	mysql_close($conn);
}
?>