<?php require_once("include/session.php"); ?>
<?php require_once("include/connection.php"); ?>
<?php require_once("include/functions.php"); ?>
<?php
if(logged_in()){
	redirect_to("table.php");
}

include_once("include/form_function.php");

if (isset($_POST['submit'])){
	$errors = array();
	// perform validations on the form data
	$required_fields = array('username', 'password');
	$errors = array_merge($errors, check_required_fields($required_fields, $_POST));

	$field_with_lenght=$_POST['username'];
	$errors=array_merge($errors, check_max_field_lengths($field_with_lenght, 50));

	$field_with_lenght2=$_POST['password'];
	$errors=array_merge($errors, check_max_field_lengths($field_with_lenght2, 30));

	// $fields_with_lengths = array('username' => 30, 'password' => 30);
	//$errors = array_merge($errors, check_max_field_lengths($fields_with_lengths, $_POST));

	$username = trim(mysql_prep($_POST['username']));
	$password = trim(mysql_prep($_POST['password']));
	$hashed_password = sha1($password);
	if ( empty($errors) ) {
		$query = "select id, username, admin, email ";
		$query.= "from php_users ";
		$query.="where username= '{$username}' ";
		$query.="and hashed_password = '{$hashed_password}';";
		$result_set=mysql_query($query,$conn);
		confirm_query($result_set);
		if(mysql_num_rows($result_set) == 1){
			$found_user=mysql_fetch_array($result_set);
			$_SESSION['user_id']=$found_user['id'];
			$_SESSION['email']=$found_user['nickname'];
			$_SESSION['username']=$found_user['name'];
			$_SESSION['admin']=$found_user['admin'];

			redirect_to("table.php");//TODO
		}else {
			$message="Username and password combination incorrect <br />
					please make sure your caps lock is off and try again";
		}
	} else {
		if (count($errors) == 1) {
			$message = "There was 1 error in the form.";
		} else {
			$message = "There were " . count($errors) . " errors in the form.";
		}
	}
}else {
	if(isset($_GET['logout']) && $_GET['logout'] == 1){
		$message="You are now logged out.";
	}
	$username="";
	$password="";
}
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="container" style="margin-top: 100px; width: 300px">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Login</h3>
			</div>
			<div class="panel-body">
			<?php
				if(!empty($message)){
				echo "<p class=\"message\">" . $message . "</p>";
			}
			?>
				<?php
				if(!empty($errors)){
				display_errors($errors);
			}
			?>
				<form action="login.php" method="post" role="form">
					<div class="form-group">
						<label for="username">Username</label> 
						<input	type="text" name="username" maxlength="30" class="form-control" id="username" placeholder="Username">
					</div>
					<div class="form-group">
						<label for="password">Password</label> 
						<input	type="password" name="password" maxlength="30" class="form-control" id="password">
					</div>
					<button type="submit" name="submit" class="btn btn-success btn-block">Login</button>
				</form>
			</div>
		</div>
	</div>

</body>
</html>

<?php
if(isset($conn)){
	mysql_close($conn);
}
?>